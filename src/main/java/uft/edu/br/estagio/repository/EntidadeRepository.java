package uft.edu.br.estagio.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uft.edu.br.estagio.model.Entidade;
import uft.edu.br.estagio.model.TipoEntidade;

import java.util.List;

@Repository //Define a classe como um bean do Spring
public interface EntidadeRepository extends JpaRepository<Entidade, Long> {
    @Query(nativeQuery = true, value="Select * from entidade as u inner join entidade_tipo up on u.id = up.id_entidade inner join tipo_entidade p on up.id_tipo_entidade = p.id where p.nome = :tipoEntidade")
    List<TipoEntidade> findByTipoEntidade(@Param("tipoEntidade") String tipoEntidade);


}
//Deve estender JpaRepository e declarar a entidade (Post) e o tipo de chave primária (Long)
