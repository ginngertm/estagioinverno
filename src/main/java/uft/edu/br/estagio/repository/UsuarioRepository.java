package uft.edu.br.estagio.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uft.edu.br.estagio.model.Usuario;

import java.util.List;
import java.util.Optional;

@Repository //Define a classe como um bean do Spring
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Optional<Usuario> findByEmail(String email);

    /*
    @Query( "select o from MyObject o where inventoryId in :ids" )
    List<MyObject> findByInventoryIds(@Param("ids") List<Long> inventoryIdList); */


    @Query(nativeQuery = true, value="Select * from usuario as u inner join usuario_perfil up on u.id = up.id_usuario inner join perfil p on up.id_perfil = p.id where p.nome = :perfil")
    List<Usuario> findByPerfil(@Param("perfil") String perfil);

    @Query(nativeQuery = true, value="Select * from usuario as u inner join usuario_perfil up on u.id = up.id_usuario inner join perfil p on up.id_perfil = p.id where p.nome = :perfil")
    List<Usuario> findByCurso(@Param("perfil") String perfil);
}
//Deve estender JpaRepository e declarar a entidade (Post) e o tipo de chave primária (Long)

