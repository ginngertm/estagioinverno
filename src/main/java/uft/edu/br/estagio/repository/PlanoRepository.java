package uft.edu.br.estagio.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uft.edu.br.estagio.model.Plano;

@Repository //Define a classe como um bean do Spring
public interface PlanoRepository extends JpaRepository<Plano, Long> { }
//Deve estender JpaRepository e declarar a entidade (Post) e o tipo de chave primária (Long)

