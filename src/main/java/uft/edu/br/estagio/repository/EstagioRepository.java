package uft.edu.br.estagio.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uft.edu.br.estagio.model.Estagio;

@Repository //Define a classe como um bean do Spring
public interface EstagioRepository extends JpaRepository<Estagio, Long> {

}
//Deve estender JpaRepository e declarar a estagio (Post) e o tipo de chave primária (Long)
