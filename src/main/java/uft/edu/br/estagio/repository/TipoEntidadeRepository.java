package uft.edu.br.estagio.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uft.edu.br.estagio.model.TipoEntidade;

import java.util.List;

@Repository //Define a classe como um bean do Spring
public interface TipoEntidadeRepository extends JpaRepository<TipoEntidade, Long> {
    //@Query(nativeQuery = true, value="");
    //List<TipoEntidade> findByTipoEntidade(@Param("tipoEntidade") String tipoEntidade);
   // List<TipoEntidade> setSinAtivoFalse(@Param("id") Long id);


    @Modifying
    @Query("UPDATE tipo_entidade set bol_ativo = 0, data_exclusao = current_timestamp where id = :id")
    Integer setSinAtivoFalse(Long id);

    @Modifying
    @Query(nativeQuery = true, value="SELECT * FROM tipo_entidade where bol_ativo = 1")
    List<TipoEntidade> findAll();

}
//Deve estender JpaRepository e declarar a entidade (Post) e o tipo de chave primária (Long)

