package uft.edu.br.estagio.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uft.edu.br.estagio.model.Curso;

import java.util.List;

@Repository //Define a classe como um bean do Spring
public interface CursoRepository extends JpaRepository<Curso, Long> {



    @Modifying
    @Query("UPDATE curso set bol_ativo = 0, data_exclusao = current_timestamp where id = :id")
    Integer setSinAtivoFalse(Long id);

    @Modifying
    @Query(nativeQuery = true, value="SELECT * FROM curso where bol_ativo = 1")
    List<Curso> findAll();
}
//Deve estender JpaRepository e declarar a curso (Post) e o tipo de chave primária (Long)
