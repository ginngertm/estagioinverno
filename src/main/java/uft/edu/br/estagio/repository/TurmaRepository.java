package uft.edu.br.estagio.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uft.edu.br.estagio.model.Turma;

import java.util.List;

@Repository //Define a classe como um bean do Spring
public interface TurmaRepository extends JpaRepository<Turma, Long> {
    @Query(nativeQuery = true, value="Select * from turma as u inner join turma_usuario up on u.id = up.id_turma inner join usuario p on up.id_usuario = p.id where p.nome = :tipoTurma")
    List<Turma> findByTurma(@Param("tipoTurma") String tipoTurma);
}
//Deve estender JpaRepository e declarar a turma (Post) e o tipo de chave primária (Long)

