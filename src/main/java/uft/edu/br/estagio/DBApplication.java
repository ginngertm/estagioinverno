package uft.edu.br.estagio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class DBApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(DBApplication.class);

    public static void main(String args[]) {
        SpringApplication.run(DBApplication.class, args);
    }

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void run(String... strings) throws Exception {

        log.info("Creating tables");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS curso(" +
                "id SERIAL PRIMARY KEY" +
                ", nome VARCHAR(255) NOT NULL" +
                ", id_coordenador integer default NULL" +
                ", id_coordenador_substituto integer default NULL" +
                ", id_secretario_coordenacao integer default NULL" +
                ", data_inclusao timestamp DEFAULT CURRENT_TIMESTAMP"+
                ", data_exclusao timestamp DEFAULT NULL"+
                ", bol_ativo integer DEFAULT '1')");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS modelo_documento(" +
                "id  SERIAL PRIMARY KEY" +
                ", titulo     VARCHAR(300) NOT NULL" +
                ", conteudo     VARCHAR(9999999) NOT NULL" +
                ", val_meses_adm_publica     VARCHAR(3) DEFAULT NULL" +
                ", val_meses_demais_entidades     VARCHAR(3) DEFAULT NULL" +
                ", instrucoes     VARCHAR(600) NOT NULL"+
                ", bol_ativo integer DEFAULT 1)");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS perfil(" +
                "id SERIAL PRIMARY KEY" +
                ", nome VARCHAR(255) NOT NULL"+
                ", sigla VARCHAR(255) NOT NULL" +
                ", bol_ativo integer DEFAULT 1)");



        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS entidade(" +
                "id  SERIAL PRIMARY KEY" +
                ", nome  VARCHAR(250) NOT NULL" +
                ", razao_social  VARCHAR(250) DEFAULT NULL" +
                ", cnpj  VARCHAR(30) DEFAULT NULL" +
                ", inscricao_estadual  VARCHAR(100) DEFAULT NULL" +
                ", ramo_atividade  VARCHAR(100) DEFAULT NULL" +
                ", endereço VARCHAR(300) DEFAULT NULL" +
                ", bairro VARCHAR(300) DEFAULT NULL" +
                ", cidade VARCHAR(150) DEFAULT NULL" +
                ", estado VARCHAR(2) DEFAULT NULL" +
                ", cep VARCHAR(20) DEFAULT NULL" +
                ", email VARCHAR(250) DEFAULT NULL" +
                ", telefone VARCHAR(250) DEFAULT NULL" +
                ", representante_legal VARCHAR(250) DEFAULT NULL" +
                ", representante_cargo VARCHAR(250) DEFAULT NULL" +
                ", bol_adm_publica INTEGER DEFAULT NULL"+
                ", data_inclusao timestamp DEFAULT CURRENT_TIMESTAMP"+
                ", data_exclusao timestamp DEFAULT NULL"+
                ", bol_ativo integer DEFAULT 1)");

        //ok
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS tipo_entidade(" +
                "id SERIAL PRIMARY KEY" +
                ", nome VARCHAR(255) NOT NULL"+
                ", data_inclusao timestamp DEFAULT CURRENT_TIMESTAMP"+
                ", data_exclusao timestamp DEFAULT NULL"+
                ", bol_ativo integer DEFAULT 1)");

        //ok
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS setor(" +
                "id SERIAL PRIMARY KEY" +
                ", nome VARCHAR(255) NOT NULL"+
                ", data_inclusao timestamp DEFAULT CURRENT_TIMESTAMP"+
                ", data_exclusao timestamp DEFAULT NULL"+
                ", bol_ativo integer DEFAULT 1)");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS disciplina(" +
                "id SERIAL PRIMARY KEY" +
                ", nome VARCHAR(255) NOT NULL"+
                ", data_inclusao timestamp DEFAULT CURRENT_TIMESTAMP"+
                ", data_exclusao timestamp DEFAULT NULL"+
                ", bol_ativo integer DEFAULT 1)");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS campus(" +
                "id SERIAL PRIMARY KEY" +
                ", nome VARCHAR(255) NOT NULL"+
                ", endereço VARCHAR(300) DEFAULT NULL" +
                ", bairro VARCHAR(300) DEFAULT NULL" +
                ", cidade VARCHAR(150) DEFAULT NULL" +
                ", estado VARCHAR(2) DEFAULT NULL" +
                ", cep VARCHAR(20) DEFAULT NULL" +
                ", email VARCHAR(250) DEFAULT NULL" +
                ", telefone VARCHAR(250) DEFAULT NULL" +
                ", representante_legal VARCHAR(250) DEFAULT NULL" +
                ", representante_cargo VARCHAR(250) DEFAULT NULL" +
                ", data_inclusao timestamp DEFAULT CURRENT_TIMESTAMP"+
                ", data_exclusao timestamp DEFAULT NULL"+
                ", bol_ativo integer DEFAULT 1)");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS turma(" +
                "id SERIAL PRIMARY KEY" +
                ", nome VARCHAR(255) NOT NULL"+
                ", codigo VARCHAR(255) default NULL"+
                ", periodo VARCHAR(255) NOT NULL"+
                ", carga_horaria VARCHAR(255) default null"+
                ", carga_horaria_ministrada VARCHAR(255) default NULL"+
                ", carga_horaria_aula VARCHAR(255) default NULL"+
                ", tipo_turma VARCHAR(255) default NULL"+
                ", creditos VARCHAR(255) default NULL"+
                ", data_inicio DATA default NULL"+
                ", data_fim DATA default NULL"+
                ", id_curso DATA default NULL"+
                ", id_campus DATA default NULL"+
                ", id_coordenador DATA default NULL"+
                ", id_aluno DATA default NULL"+
                ", bol_ativo integer DEFAULT 1)");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS usuario(" +
                "id  SERIAL PRIMARY KEY\n" +
                ", nome     VARCHAR(250) NOT NULL\n" +
                ", matricula VARCHAR(30) NOT NULL"+
                ", cpf  VARCHAR(30) DEFAULT NULL" +
                ", endereço VARCHAR(300) DEFAULT NULL" +
                ", bairro VARCHAR(300) DEFAULT NULL" +
                ", cidade VARCHAR(150) DEFAULT NULL" +
                ", estado VARCHAR(2) DEFAULT NULL" +
                ", cep VARCHAR(20) DEFAULT NULL" +
                ", email VARCHAR(250) DEFAULT NULL" +
                ", senha VARCHAR(255) DEFAULT 'UFTESTAGIO'" +
                ", telefone VARCHAR(250) DEFAULT NULL" +
                ", whatsapp VARCHAR(100) DEFAULT NULL" +
                ", skype VARCHAR(100) DEFAULT NULL" +
                ", representante_legal VARCHAR(250) DEFAULT NULL" +
                ", bol_ativo integer DEFAULT 1)");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS estagio(" +
                "id SERIAL PRIMARY KEY" +
                ", vaga varchar(255) DEFAULT NULL"+
                ", data_inicio DATE DEFAULT NULL"+
                ", data_fim DATE DEFAULT NULL"+
                ", frequencia varchar DEFAULT 0"+
                ", total_presenca int DEFAULT 0"+
                ", total_faltas int DEFAULT 0"+
                ", nota1 float DEFAULT 0"+
                ", nota2 float DEFAULT 0"+
                ", media float DEFAULT 0"+
                ", exame float DEFAULT 0"+
                ", media_final float DEFAULT 0"+
                ", situacao varchar(255) DEFAULT 0"+
                ", bol_ativo integer DEFAULT 1)");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS plano(" +
                "id SERIAL PRIMARY KEY" +
                ", nome VARCHAR(255) NOT NULL"+
                ", data_inicio DATE DEFAULT NULL"+
                ", data_fim DATE NOT NULL"+
                ", bol_concluido integer DEFAULT 0"+
                ", bol_ativo integer DEFAULT 1)");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS atividade(" +
                "id SERIAL PRIMARY KEY" +
                ", nome VARCHAR(255) NOT NULL"+
                ", relatorio VARCHAR(255) NOT NULL"+
                ", data_inicio VARCHAR(15) DEFAULT NULL"+
                ", data_fim VARCHAR(15) default NULL"+
                ", bol_concluida integer DEFAULT 0"+
                ", visto_supervisor integer DEFAULT 0"+
                ", visto_orientador integer DEFAULT 0"+
                ", data_inclusao timestamp DEFAULT CURRENT_TIMESTAMP"+
                ", data_exclusao timestamp DEFAULT NULL"+
                ", bol_ativo integer DEFAULT 1)");



        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS estagio_aluno(" +
                "id_estagio int REFERENCES estagio (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_aluno int REFERENCES usuario (id) ON UPDATE CASCADE"+
                ", CONSTRAINT estagio_aluno_pkey PRIMARY KEY (id_estagio, id_aluno))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS estagio_supervisor(" +
                "id_estagio int REFERENCES estagio (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_supervisor int REFERENCES usuario (id) ON UPDATE CASCADE"+
                ", CONSTRAINT estagio_supervisor_pkey PRIMARY KEY (id_estagio, id_supervisor))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS estagio_orientador(" +
                "id_estagio int REFERENCES estagio (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_orientador int REFERENCES usuario (id) ON UPDATE CASCADE"+
                ", CONSTRAINT estagio_orientador_pkey PRIMARY KEY (id_estagio, id_orientador))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS estagio_turma(" +
                "id_estagio int REFERENCES estagio (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_turma int REFERENCES turma (id) ON UPDATE CASCADE"+
                ", CONSTRAINT estagio_turma_pkey PRIMARY KEY (id_estagio, id_turma))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS estagio_setor(" +
                "id_estagio int REFERENCES estagio (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_setor int REFERENCES setor (id) ON UPDATE CASCADE"+
                ", CONSTRAINT estagio_setor_pkey PRIMARY KEY (id_estagio, id_setor))");


        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS plano_atividade(" +
                "id_plano int REFERENCES plano (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_atividade int REFERENCES atividade (id) ON UPDATE CASCADE"+
                ", CONSTRAINT plano_atividade_pkey PRIMARY KEY (id_plano, id_atividade))");

        //CORRIGIR: CRIAR TURMA PARA CURSO, VINCULAR ALUNO AO CURSO E PLANO A ALUNO_TURMA
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS estagio_plano(" +
                "id_estagio int REFERENCES estagio (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_plano int REFERENCES plano(id) ON UPDATE CASCADE"+
                ", CONSTRAINT estagio_plano_pkey PRIMARY KEY (id_estagio, id_plano))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS permissao(" +
                "id SERIAL PRIMARY KEY" +
                ", nome VARCHAR(255) NOT NULL"+
                ", bol_ativo integer DEFAULT 1)");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS perfil_permissao(" +
                "id_perfil int REFERENCES perfil (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ", id_permissao int REFERENCES permissao (id) ON UPDATE CASCADE"+
                ", CONSTRAINT perfil_permissao_pkey PRIMARY KEY (id_perfil, id_permissao))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS usuario_perfil(" +
                "id_usuario int REFERENCES usuario (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ", id_perfil int REFERENCES perfil (id) ON UPDATE CASCADE"+
                ", CONSTRAINT usuario_perfil_pkey PRIMARY KEY (id_usuario, id_perfil))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS usuario_curso(" +
                "id_usuario int REFERENCES usuario (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_curso int REFERENCES curso (id) ON UPDATE CASCADE"+
                ", CONSTRAINT usuario_curso_pkey PRIMARY KEY (id_usuario, id_curso))");


        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS turma_usuario(" +
                "id_turma int REFERENCES turma (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_usuario int REFERENCES usuario (id) ON UPDATE CASCADE"+
                ", CONSTRAINT turma_usuario_pkey PRIMARY KEY (id_turma, id_usuario))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS turma_campus(" +
                "id_turma int REFERENCES turma (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_campus int REFERENCES campus (id) ON UPDATE CASCADE"+
                ", CONSTRAINT turma_campus_pkey PRIMARY KEY (id_turma, id_campus))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS turma_curso(" +
                "id_turma int REFERENCES turma (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_curso int REFERENCES curso (id) ON UPDATE CASCADE"+
                ", CONSTRAINT turma_curso_pkey PRIMARY KEY (id_turma, id_curso))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS turma_disciplina(" +
                "id_turma int REFERENCES turma (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_disciplina int REFERENCES disciplina (id) ON UPDATE CASCADE"+
                ", CONSTRAINT estagio_turma_pkey PRIMARY KEY (id_turma, id_disciplina))");



        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS setor_entidade(" +
                "id_setor int REFERENCES setor (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_entidade int REFERENCES entidade(id) ON UPDATE CASCADE"+
                ", CONSTRAINT setor_entidade_pkey PRIMARY KEY (id_setor, id_entidade))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS setor_campus(" +
                "id_setor int REFERENCES setor (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_campus int REFERENCES entidade(id) ON UPDATE CASCADE"+
                ", CONSTRAINT setor_campus_pkey PRIMARY KEY (id_setor, id_campus))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS setor_usuario(" +
                "id_setor int REFERENCES setor (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_usuario int REFERENCES usuario(id) ON UPDATE CASCADE"+
                ", CONSTRAINT setor_usuario_pkey PRIMARY KEY (id_setor, id_usuario))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS entidade_tipo(" +
                "id_entidade int REFERENCES entidade (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_tipo_entidade int REFERENCES tipo_entidade (id) ON UPDATE CASCADE"+
                ", CONSTRAINT entidade_tipo_pkey PRIMARY KEY (id_entidade, id_tipo_entidade))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS entidade_campus(" +
                "id_entidade int REFERENCES entidade (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_campus int REFERENCES campus (id) ON UPDATE CASCADE"+
                ", CONSTRAINT entidade_campus_pkey PRIMARY KEY (id_entidade, id_campus))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS campus_curso(" +
                "id_campus_curso  SERIAL PRIMARY KEY " +
                ",id_campus int REFERENCES entidade (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ",id_curso int REFERENCES campus (id) ON UPDATE CASCADE)"
                );



        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS entidade_perfil(" +
                "id_entidade int REFERENCES entidade (id) ON UPDATE CASCADE ON DELETE CASCADE"+
                ", id_perfil int REFERENCES perfil (id) ON UPDATE CASCADE"+
                ", CONSTRAINT entidade_perfil_pkey PRIMARY KEY (id_entidade, id_perfil))");


        log.info("Inserting data");

        jdbcTemplate.execute("do $$ begin IF NOT EXISTS (SELECT * FROM perfil LIMIT 1) THEN "+
                "insert into perfil(id, nome, sigla, bol_ativo) values"+
                " (nextval('perfil_seq'),'Administrador','ADM', 1)" +
                ", (nextval('perfil_seq'),'Estagiário','EST', 1)" +
                ", (nextval('perfil_seq'),'Orientador','ORI', 1)" +
                ", (nextval('perfil_seq'),'Supervisor','SUP', 1)" +
                ", (nextval('perfil_seq'),'Unidade Concedente','UNC', 1)" +
                ", (nextval('perfil_seq'),'Instituição de Ensino','IES', 1)" +
                ", (nextval('perfil_seq'),'Central de Estagio','CES', 1);" +
                " END IF; end $$");

        jdbcTemplate.execute("do $$ begin IF NOT EXISTS (SELECT * FROM tipo_entidade LIMIT 1) THEN "+
                "insert into tipo_entidade(id, nome, bol_ativo) values"+
                " (nextval('tipo_entidade_seq'),'Instituição de Ensino', 1)" +
                ", (nextval('tipo_entidade_seq'),'Unidade Concedente', 1);" +
                " END IF; end $$");

        jdbcTemplate.execute("do $$ begin IF NOT EXISTS (SELECT * FROM campus LIMIT 1) THEN "+
                "insert into campus(id, nome, bol_ativo) values"+
                " (nextval('campus_seq'),'ARAGUAÍNA', 1)" +
                ", (nextval('campus_seq'),'ARRAIAS', 1)" +
                ", (nextval('campus_seq'),'GURUPI', 1)" +
                ", (nextval('campus_seq'),'MIRACEMA', 1)" +
                ", (nextval('campus_seq'),'PALMAS', 1)" +
                ", (nextval('campus_seq'),'PORTO NACIONAL', 1)" +
                ", (nextval('campus_seq'),'TOCANTINÓPOLIS', 1);" +
                " END IF; end $$");

        jdbcTemplate.execute("do $$ begin IF NOT EXISTS (SELECT * FROM curso LIMIT 1) THEN "+
                "insert into curso(id,nome, campus) values(nextval('curso_seq'),'Zootecnia','Araguaína')" +
                "                                        , (nextval('curso_seq'),'Química','Araguaína')" +
                "                                        , (nextval('curso_seq'),'Medicina Veterinária','Araguaína')" +
                "                                        , (nextval('curso_seq'),'Medicina','Araguaína')" +
                "                                        , (nextval('curso_seq'),'Matemática','Araguaína')" +
                "                                        , (nextval('curso_seq'),'Logística','Araguaína')" +
                "                                        , (nextval('curso_seq'),'Letras – Língua Inglesa, Portuguesa e suas Respectivas Literaturas','Araguaína')" +
                "                                        , (nextval('curso_seq'),'História','Araguaína')" +
                "                                        , (nextval('curso_seq'),'Gestão de Turismo','Araguaína')" +
                "                                        , (nextval('curso_seq'),'Gestão de Cooperativas','Araguaína')" +
                "                                        , (nextval('curso_seq'),'Geografia','Araguaína')" +
                "                                        , (nextval('curso_seq'),'Física','Araguaína')" +
                "                                        , (nextval('curso_seq'),'Biologia','Araguaína')" +
                "                                        , (nextval('curso_seq'),'Turismo Patrimonial e Socioambiental','Arraias')" +
                "                                        , (nextval('curso_seq'),'Pedagogia','Arraias')" +
                "                                        , (nextval('curso_seq'),'Matemática','Arraias')" +
                "                                        , (nextval('curso_seq'),'Educação do Campo','Arraias')" +
                "                                        , (nextval('curso_seq'),'Química Ambiental','Gurupi')" +
                "                                        , (nextval('curso_seq'),'Engenharia Florestal',' Gurupi')" +
                "                                        , (nextval('curso_seq'),'Engenharia de Bioprocessos e Biotecnologia','Gurupi')" +
                "                                        , (nextval('curso_seq'),'Agronomia','Gurupi')" +
                "                                        , (nextval('curso_seq'),'Serviço Social','Miracema')" +
                "                                        , (nextval('curso_seq'),'Psicologia','Miracema')" +
                "                                        , (nextval('curso_seq'),'Pedagogia','Miracema')" +
                "                                        , (nextval('curso_seq'),'Educação Física','Miracema')" +
                "                                        , (nextval('curso_seq'),'Teatro','Palmas')" +
                "                                        , (nextval('curso_seq'),'Pedagogia','Palmas')" +
                "                                        , (nextval('curso_seq'),'Nutrição','Palmas')" +
                "                                        , (nextval('curso_seq'),'Medicina','Palmas')" +
                "                                        , (nextval('curso_seq'),'Jornalismo','Palmas')" +
                "                                        , (nextval('curso_seq'),'Filosofia','Palmas')" +
                "                                        , (nextval('curso_seq'),'Engenharia Elétrica','Palmas')" +
                "                                        , (nextval('curso_seq'),'Engenharia de Alimentos','Palmas')" +
                "                                        , (nextval('curso_seq'),'Engenharia Civil','Palmas')" +
                "                                        , (nextval('curso_seq'),'Engenharia Ambiental','Palmas')" +
                "                                        , (nextval('curso_seq'),'Enfermagem','Palmas')" +
                "                                        , (nextval('curso_seq'),'Direito','Palmas')" +
                "                                        , (nextval('curso_seq'),'Ciências Econômicas','Palmas')" +
                "                                        , (nextval('curso_seq'),'Ciências Contábeis','Palmas')" +
                "                                        , (nextval('curso_seq'),'Ciência da Computação','Palmas')" +
                "                                        , (nextval('curso_seq'),'Arquitetura e Urbanismo','Palmas')" +
                "                                        , (nextval('curso_seq'),'Administração','Palmas')" +
                "                                        , (nextval('curso_seq'),'Relações Internacionais','Porto Nacional')" +
                "                                        , (nextval('curso_seq'),'Letras – Língua Portuguesa e suas respectivas literaturas','Porto Nacional')" +
                "                                        , (nextval('curso_seq'),'Letras ‒ Libras','Porto Nacional')" +
                "                                        , (nextval('curso_seq'),'História','Porto Nacional')" +
                "                                        , (nextval('curso_seq'),'Geografia','Porto Nacional')" +
                "                                        , (nextval('curso_seq'),'Ciências Sociais','Porto Nacional')" +
                "                                        , (nextval('curso_seq'),'Ciências Biológicas','Porto Nacional')" +
                "                                        , (nextval('curso_seq'),'Pedagogia','Tocantinópolis')" +
                "                                        , (nextval('curso_seq'),'Educação Física','Tocantinópolis')" +
                "                                        , (nextval('curso_seq'),'Educação do Campo','Tocantinópolis')" +
                "                                        , (nextval('curso_seq'),'Ciências Sociais','Tocantinópolis');" +
                " END IF; end $$");

    }


}