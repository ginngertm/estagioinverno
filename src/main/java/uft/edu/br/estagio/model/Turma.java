package uft.edu.br.estagio.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity (name = "turma") //Define o nome da tabela que será criada no banco de dados
public class Turma implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "turma_seq", sequenceName = "turma_seq") //Cria uma sequence para ser usada com a tabela
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "turma_seq")
    //Define que a tabela fará uso da sequence criada antes
    private Long id;

    @Column(nullable = false, length = 250) //Define propriedades da coluna
    @NotBlank(message = "Infome um nome para o turma")
    //Define qual mensagem será exibida caso a validação da coluna falhar
    private String nome;

    @Column(name = "bol_ativo")
    private Integer bolAtivo = 1;


    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "turma_usuario",
            joinColumns = {@JoinColumn(name = "id_turma")},
            inverseJoinColumns = {@JoinColumn(name = "id_usuario")})
    private Set<Usuario> alunos = new HashSet<>();



    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "turma_curso",
            joinColumns = {@JoinColumn(name = "id_turma")},
            inverseJoinColumns = {@JoinColumn(name = "id_curso")})

    private Set<Curso> cursos = new HashSet<>();


    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "turma_disciplina",
            joinColumns = {@JoinColumn(name = "id_turma")},
            inverseJoinColumns = {@JoinColumn(name = "id_disciplina")})

    private Set<Disciplina> disciplinas = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "turma_campus",
            joinColumns = {@JoinColumn(name = "id_turma")},
            inverseJoinColumns = {@JoinColumn(name = "id_campus")})

    private Set<Campus> campi = new HashSet<>();


/*
    @NotNull
    @NotEmpty
    private String password;
    private String matchingPassword;

    @NotNull
    @NotEmpty
    @ValidEmail
    @NotNull
    @NotEmpty
    private String email;
*/

    /*
        @ManyToMany(fetch = FetchType.LAZY,
                cascade = {
                        CascadeType.PERSIST,
                        CascadeType.MERGE
                })
        @JoinTable(name = "turma_perfil",
                joinColumns = { @JoinColumn(name = "id_turma") },
                inverseJoinColumns = { @JoinColumn(name = "id_perfil") })

        private Set<Perfil> perfis = new HashSet<>();

        @ManyToMany(fetch = FetchType.LAZY,
                cascade = {
                        CascadeType.PERSIST,
                        CascadeType.MERGE
                })
        @JoinTable(name = "turma_curso",
                joinColumns = { @JoinColumn(name = "id_turma") },
                inverseJoinColumns = { @JoinColumn(name = "id_curso") })
        private Set<Curso> cursos = new HashSet<>();
    */
    public Turma() {
        super();
    }

    public Turma(Long id,
                 String nome,
                 Integer bolAtivo,
                 Set<Usuario> alunos,
                 Set<Curso> cursos,
                 Set<Campus> campi,
                 Set<Disciplina> disciplinas
    ) {
        super();
        this.id = id;
        this.nome = nome;
        this.bolAtivo = bolAtivo;
        this.alunos = alunos;
        this.cursos = cursos;
        this.campi = campi;
        this.disciplinas = disciplinas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getBolAtivo() {
        return bolAtivo;
    }

    public void setBolAtivo(Integer bolAtivo) {
        this.bolAtivo = bolAtivo;
    }


    //foreign keys

    public Set<Usuario> getAlunos() {
        return alunos;
    }

    public void setAlunos(Set<Usuario> alunos) {
        this.alunos = alunos;
    }

    public Set<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(Set<Curso> cursos) {
        this.cursos = cursos;
    }

    public Set<Campus> getCampi() {
        return campi;
    }

    public void setCampi(Set<Campus> campi) {
        this.campi = campi;
    }

    public Set<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(Set<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }



}
