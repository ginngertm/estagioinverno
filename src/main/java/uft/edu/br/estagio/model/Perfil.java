package uft.edu.br.estagio.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity (name = "perfil") //Define o nome da tabela que será criada no banco de dados
public class Perfil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "perfil_seq", sequenceName = "perfil_seq") //Cria uma sequence para ser usada com a tabela
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "perfil_seq") //Define que a tabela fará uso da sequence criada antes
    private Long id;

    @Column(nullable = false, length = 150) //Define propriedades da coluna
    @NotBlank(message = "Infome um nome para o perfil") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String nome;

    @Column(nullable = false, length = 150) //Define propriedades da coluna
    @NotBlank(message = "Infome uma sigla para o perfil") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String sigla;

    @Column( length = 1, name = "bol_ativo") //Define propriedades da coluna
    private Integer bolAtivo = 1;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "perfis")

    private Set<Usuario> usuarios = new HashSet<>();

   // @Column( length = 1, name = "bol_ativo") //Define propriedades da coluna
   // private Integer bolAtivo = 1;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "perfil_permissao",
            joinColumns = { @JoinColumn(name = "id_perfil") },
            inverseJoinColumns = { @JoinColumn(name = "id_permissoes") })

    private Set<Permissao> permissoes = new HashSet<>();



    public Perfil()
    {
        super();
    }

    public Perfil(Long id, String nome, String sigla, Set<Usuario> usuarios, Set<Permissao> permissoes)
    {
        super();
        this.id = id;
        this.nome = nome;
        this.usuarios = usuarios;
        this.permissoes = permissoes;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getBolAtivo() {
        return bolAtivo;
    }

    public void setBolAtivo(Integer bolAtivo) {
        this.bolAtivo = bolAtivo;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public Set<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Set<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public Set<Permissao> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(Set<Permissao> permissoes) {
        this.permissoes = permissoes;
    }


    @Override
    public String toString()
    {
        return "Perfil [id=" + id + ", nome=" + nome + ", usuarios=" + usuarios + ", permissoes=" + permissoes + "]";
    }
}