package uft.edu.br.estagio.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity (name = "permissao") //Define o nome da tabela que será criada no banco de dados
public class Permissao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "permissao_seq", sequenceName = "permissao_seq") //Cria uma sequence para ser usada com a tabela
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "permissao_seq") //Define que a tabela fará uso da sequence criada antes
    private Long id;

    @Column(nullable = false, length = 150) //Define propriedades da coluna
    @NotBlank(message = "Infome um nome para o permissao") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String nome;
    
    @Column( length = 1, name = "bol_ativo") //Define propriedades da coluna
    private Integer bolAtivo = 1;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "permissoes")

    private Set<Perfil> perfis = new HashSet<>();


    public Permissao()
    {
        super();
    }

    public Permissao(Long id, String nome, Integer bolAtivo,  Set<Perfil> perfis)
    {
        super();
        this.id = id;
        this.nome = nome;
        this.bolAtivo = bolAtivo;
        this.perfis = perfis;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getBolAtivo() {
        return bolAtivo;
    }

    public void setBolAtivo(Integer bolAtivo) {
        this.bolAtivo = bolAtivo;
    }


    public Set<Perfil> getPerfis() {
        return perfis;
    }

    public void setPerfis(Set<Perfil> perfis) {
        this.perfis = perfis;
    }


    @Override
    public String toString()
    {
        return "Permissao [id=" + id + ", nome=" + nome + "]";
    }
}