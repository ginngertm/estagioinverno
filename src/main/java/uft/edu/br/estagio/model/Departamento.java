package uft.edu.br.estagio.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity (name = "departamento") //Define o nome da tabela que será criada no banco de dados
public class Departamento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "departamento_seq", sequenceName = "departamento_seq") //Cria uma sequence para ser usada com a tabela
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "departamento_seq") //Define que a tabela fará uso da sequence criada antes
    private Long id;

    @Column(nullable = false, length = 150) //Define propriedades da coluna
    @NotBlank(message = "Infome um nome para o departamento") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String nome;

    private String endereço;
    private String bairro;
    private String cidade;
    private String estado;
    private String cep;
    private String email;
    private String telefone;
    @Column(name = "representante_legal")
    private String representanteLegal;
    @Column(name = "representante_cargo")
    private String representanteCargo;

    @Column( length = 1, name = "bol_ativo") //Define propriedades da coluna
    private Integer bolAtivo = 1;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "entidade_departamento",
            joinColumns = { @JoinColumn(name = "id_departamento") },
            inverseJoinColumns = { @JoinColumn(name = "id_entidade") })

    private Set<Entidade> entidades = new HashSet<>();



    //mappedBy = "perfis"
    public Departamento()
    {
        super();
    }

    public Departamento(Long id,
                        String nome,
                        String endereço,
                        String bairro,
                        String cidade,
                        String estado,
                        String cep,
                        String email,
                        String telefone,
                        String representanteLegal,
                        String representanteCargo,
                        Set<Entidade> entidades
                        )
    {
        super();
        this.id = id;
        this.nome = nome;
        this.endereço = endereço;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.cep = cep;
        this.email = email;
        this.telefone = telefone;
        this.representanteLegal = representanteLegal;
        this.representanteCargo = representanteCargo;
        this.entidades = entidades;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereço(){return endereço;}
    public void setEndereço(String endereço){this.endereço = endereço;}

    public String getBairro(){return bairro;}
    public void setBairro(String bairro){this.bairro = bairro;}

    public String getCidade(){return cidade;}
    public void setCidade(String cidade){this.cidade = cidade;}

    public String getEstado(){return estado;}
    public void setEstado(String estado){this.estado = estado;}

    public String getCep(){return cep;}
    public void setCep(String cep){this.cep = cep;}

    public String getEmail(){return email;}
    public void setEmail(String email){this.email = email;}

    public String getTelefone(){return telefone;}
    public void setTelefone(String telefone){this.telefone = telefone;}

    public String getRepresentanteLegal(){return representanteLegal;}
    public void setRepresentanteLegal(String representanteLegal){this.representanteLegal = representanteLegal;}

    public String getRepresentanteCargo(){return representanteCargo;}
    public void setRepresentanteCargo(String representanteCargo){this.representanteCargo = representanteCargo;}

    public Integer getBolAtivo() {
        return bolAtivo;
    }

    public void setBolAtivo(Integer bolAtivo) {
        this.bolAtivo = bolAtivo;
    }

    public Set<Entidade> getEntidades() {
        return entidades;
    }
    public void setEntidades(Set<Entidade> entidades) {
        this.entidades = entidades;
    }

    @Override
    public String toString()
    {
        return "Departamento [id=" + id + ", nome=" + nome + ", entidades=" + entidades + "]";
    }
}