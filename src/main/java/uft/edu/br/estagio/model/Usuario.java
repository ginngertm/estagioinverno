package uft.edu.br.estagio.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity (name = "usuario") //Define o nome da tabela que será criada no banco de dados
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "usuario_seq", sequenceName = "usuario_seq") //Cria uma sequence para ser usada com a tabela
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuario_seq") //Define que a tabela fará uso da sequence criada antes
    private Long id;

    @Column(nullable = false, length = 150) //Define propriedades da coluna
    @NotBlank(message = "Infome um nome para o usuario") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String nome;

    @Column(name = "bol_ativo")
    private Integer bolAtivo = 1;

    private String matricula;
   
    private String cpf;
   
    private String endereço;
   
    private String bairro;
   
    private String cidade;

    private String estado;

    private String cep;

    private String email;

    private String telefone;
    
    private String whatsapp;

    private String skype;
   
    private String representante_legal;


/*
    @NotNull
    @NotEmpty
    private String password;
    private String matchingPassword;

    @NotNull
    @NotEmpty
    @ValidEmail
    @NotNull
    @NotEmpty
    private String email;
*/
    @Column(nullable = false, length = 255)
    private String senha;


    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "usuario_perfil",
            joinColumns = { @JoinColumn(name = "id_usuario") },
            inverseJoinColumns = { @JoinColumn(name = "id_perfil") })

    private Set<Perfil> perfis = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "usuario_curso",
            joinColumns = { @JoinColumn(name = "id_usuario") },
            inverseJoinColumns = { @JoinColumn(name = "id_curso") })
    private Set<Curso> cursos = new HashSet<>();


    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "alunos"
           )
    private Set<Turma> turmas = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "usuarios"
    )
    private Set<Setor> setores = new HashSet<>();




    public Usuario()
    {
        super();
    }

    public Usuario(Long id,
                   String nome,
                   String matricula,
                   String cpf,
                   String endereço,
                   String bairro,
                   String cidade,
                   String estado,
                   String cep,
                   String email,
                   String senha,
                   String telefone,
                   String whatsapp,
                   String skype,
                   String representante_legal,
                   Integer bolAtivo,
                   Set <Perfil> perfis,
                   Set <Turma> turmas,
                   Set<Curso> cursos,
                   Set<Setor> setores
                   )
    {
        super();
        this.id = id;
        this.nome = nome;
        this.matricula = matricula;
        this.cpf = cpf;
        this.endereço = endereço;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.cep = cep;
        this.email = email;
        this.senha = senha;
        this.telefone = telefone;
        this.whatsapp = whatsapp;
        this.skype = skype;
        this.representante_legal = representante_legal;
        this.bolAtivo = bolAtivo;
        this.perfis = perfis;
        this.cursos = cursos;
        this.turmas = turmas;
        this.setores = setores;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }


    public String getNome(){return nome;}
    public void setNome(String nome){this.nome = nome;}

    public String getMatricula(){return matricula;}
    public void setMatricula(String matricula){this.matricula = matricula;}

    public String getCpf(){return cpf;}
    public void setCpf(String cpf){this.cpf = cpf;}

    public String getEndereço(){return endereço;}
    public void setEndereço(String endereço){this.endereço = endereço;}

    public String getBairro(){return bairro;}
    public void setBairro(String bairro){this.bairro = bairro;}

    public String getCidade(){return cidade;}
    public void setCidade(String cidade){this.cidade = cidade;}

    public String getEstado(){return estado;}
    public void setEstado(String estado){this.estado = estado;}

    public String getCep(){return cep;}
    public void setCep(String cep){this.cep = cep;}

    public String getEmail(){return email;}
    public void setEmail(String email){this.email = email;}

    public String getSenha(){return senha;}
    public void setSenha(String senha){this.senha = senha;}

    public String getTelefone(){return telefone;}
    public void setTelefone(String telefone){this.telefone = telefone;}

    public String getWhatsapp(){return whatsapp;}
    public void setWhatsapp(String whatsapp){this.whatsapp = whatsapp;}

    public String getSkype(){return skype;}
    public void setSkype(String skype){this.skype = skype;}

    public String getRepresentante_legal(){return representante_legal;}
    public void setRepresentante_legal(String representante_legal){this.representante_legal = representante_legal;}

    public Integer getBolAtivo(){return bolAtivo;}
    public void setBolAtivo(Integer bolAtivo){this.bolAtivo = bolAtivo;}


    //foreign keys

    public Set<Perfil> getPerfis() {
        return perfis;
    }

    public void setPerfis(Set<Perfil> perfis) {
        this.perfis = perfis;
    }

    public Set<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(Set<Curso> cursos) {
        this.cursos = cursos;
    }

    public Set<Turma> getTurmas() {
        return turmas;
    }

    public void setTurmas(Set<Turma> turma) {
        this.turmas = turmas;
    }


    public Set<Setor> getSetores() {
        return setores;
    }

    public void setSetores(Set<Setor> setores) {
        this.setores = setores;
    }


}