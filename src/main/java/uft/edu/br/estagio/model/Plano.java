package uft.edu.br.estagio.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity (name = "plano") //Define o nome da tabela que será criada no banco de dados
public class Plano implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "plano_seq", sequenceName = "plano_seq") //Cria uma sequence para ser usada com a tabela
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "plano_seq") //Define que a tabela fará uso da sequence criada antes
    private Long id;

    @Column(nullable = false, length = 150) //Define propriedades da coluna
    @NotBlank(message = "Infome um nome para o plano") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String nome;

    @Column( length = 1, name = "bol_ativo") //Define propriedades da coluna
    private Integer bolAtivo = 1;


   // @Column( length = 1, name = "bol_ativo") //Define propriedades da coluna
   // private Integer bolAtivo = 1;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "estagio_plano",
            joinColumns = { @JoinColumn(name = "id_plano") },
            inverseJoinColumns = { @JoinColumn(name = "id_estagio") })

    private Set<Estagio> estagios = new HashSet<>();


    public Plano()
    {
        super();
    }

    public Plano(Long id, String nome, Set<Estagio> estagios)
    {
        super();
        this.id = id;
        this.nome = nome;
        this.estagios = estagios;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getBolAtivo() {
        return bolAtivo;
    }

    public void setBolAtivo(Integer bolAtivo) {
        this.bolAtivo = bolAtivo;
    }

    public Set<Estagio> getEstagios() {
        return estagios;
    }

    public void setEstagios(Set<Estagio> estagios) {
        this.estagios = estagios;
    }




    @Override
    public String toString()
    {
        return "Plano [id=" + id + ", nome=" + nome + ", estagios=" + estagios + "]";
    }
}