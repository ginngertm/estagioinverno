package uft.edu.br.estagio.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;


@Entity (name = "modeloDocumento") //Define o nome da tabela que será criada no banco de dados
public class ModeloDocumento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "modeloDocumento_seq", sequenceName = "modeloDocumento_seq") //Cria uma sequence para ser usada com a tabela
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "modeloDocumento_seq") //Define que a tabela fará uso da sequence criada antes
    private Long id;

    @Column(nullable = false, length = 300) //Define propriedades da coluna
    @NotBlank(message = "Infome um título para o modeloDocumento") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String titulo;

    @Column(nullable = false, length = 30000) //Define propriedades da coluna
    @NotBlank(message = "Infome um conteudo para o modeloDocumento") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String conteudo;

    @Column(name="val_meses_adm_publica") //Define propriedades da coluna
    private Integer valAdmPublicaFederal;

    @Column(name="val_meses_demais_entidades") //Define propriedades da coluna
    private Integer valDemaisEntidades;

    @Column(nullable = false, length = 600) //Define propriedades da coluna
    @NotBlank(message = "Defina instruções para o correto preenchimento deste documento") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String instrucoes;


    public ModeloDocumento()
    {
        super();
    }

    public ModeloDocumento(Long id, String titulo, String conteudo, String instrucoes, Integer valAdmPublicaFederal, Integer valDemaisEntidades)
    {
        super();
        this.id = id;
        this.titulo = titulo;
        this.conteudo = conteudo;
        this.instrucoes = instrucoes;
        this.valAdmPublicaFederal = valAdmPublicaFederal;
        this.valDemaisEntidades = valDemaisEntidades;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public String getInstrucoes() {
        return instrucoes;
    }

    public void setInstrucoes(String instrucoes) {
        this.instrucoes = instrucoes;
    }

    public Integer getValAdmPublicaFederal() {
        return valAdmPublicaFederal;
    }

    public void setValAdmPublicaFederal(Integer valAdmPublicaFederal) {
        this.valAdmPublicaFederal = valAdmPublicaFederal;
    }

    public Integer getValDemaisEntidades() {
        return valDemaisEntidades;
    }

    public void setValDemaisEntidades(Integer valDemaisEntidades) {
        this.valDemaisEntidades = valDemaisEntidades;
    }


    @Override
    public String toString()
    {
        return "ModeloDocumento [id=" + id + ", titulo=" + titulo + ", conteúdo=" + conteudo+ ", instrução=" + instrucoes+ ", Validade Adm. Pública=" + valAdmPublicaFederal+ ", Validade Demais Entidades=" + valDemaisEntidades + "]";
    }
}