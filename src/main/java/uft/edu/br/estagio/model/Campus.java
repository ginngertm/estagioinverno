package uft.edu.br.estagio.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Entity (name = "campus") //Define o nome da tabela que será criada no banco de dados
public class Campus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "campus_seq", sequenceName = "campus_seq") //Cria uma sequence para ser usada com a tabela
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "campus_seq") //Define que a tabela fará uso da sequence criada antes
    private Long id;

    @Column(nullable = false, length = 150) //Define propriedades da coluna
    @NotBlank(message = "Infome um nome para o campus") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String nome;

    private String endereço;
    private String bairro;
    private String cidade;
    private String estado;
    private String cep;
    private String email;
    private String telefone;
    @Column(name = "representante_legal")
    private String representanteLegal;
    @Column(name = "representante_cargo")
    private String representanteCargo;

    @Column(name = "data_inclusao")
    private LocalDateTime dataInclusao;

    @Column(name = "data_exclusao")
    private LocalDateTime dataExclusao;

    @Column( length = 1, name = "bol_ativo") //Define propriedades da coluna
    private Integer bolAtivo = 1;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "entidade_campus",
            joinColumns = { @JoinColumn(name = "id_campus") },
            inverseJoinColumns = { @JoinColumn(name = "id_entidade") })

    private Set<Entidade> entidades = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "campi")
    private Set<Setor> setores = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })

    @JoinTable(name = "campus_curso",
            joinColumns = { @JoinColumn(name = "id_campus") },
            inverseJoinColumns = { @JoinColumn(name = "id_curso") })

    private Set<Curso> cursos = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "turma_campus",
            joinColumns = { @JoinColumn(name = "id_campus") },
            inverseJoinColumns = { @JoinColumn(name = "id_turma") })
    private Set<Turma> turmas = new HashSet<>();


    //mappedBy = "perfis"
    public Campus()
    {
        super();
    }

    public Campus(Long id,
            String nome,
            String endereço,
            String bairro,
            String cidade,
            String estado,
            String cep,
            String email,
            String telefone,
            String representanteLegal,
            String representanteCargo,
            LocalDateTime dataInclusao,
            LocalDateTime dataExclusao,
            Set<Entidade> entidades,
            Set<Curso> cursos,
            Set<Turma> turmas,
            Set<Setor> setores)
    {
        super();
        this.id = id;
        this.nome = nome;
        this.endereço = endereço;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.cep = cep;
        this.email = email;
        this.telefone = telefone;
        this.representanteLegal = representanteLegal;
        this.representanteCargo = representanteCargo;
        this.entidades = entidades;
        this.dataInclusao = dataInclusao;
        this.dataExclusao = dataExclusao;
        this.cursos = cursos;
        this.turmas = turmas;
        this.setores = setores;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereço(){return endereço;}
    public void setEndereço(String endereço){this.endereço = endereço;}

    public String getBairro(){return bairro;}
    public void setBairro(String bairro){this.bairro = bairro;}

    public String getCidade(){return cidade;}
    public void setCidade(String cidade){this.cidade = cidade;}

    public String getEstado(){return estado;}
    public void setEstado(String estado){this.estado = estado;}

    public String getCep(){return cep;}
    public void setCep(String cep){this.cep = cep;}

    public String getEmail(){return email;}
    public void setEmail(String email){this.email = email;}

    public String getTelefone(){return telefone;}
    public void setTelefone(String telefone){this.telefone = telefone;}

    public String getRepresentanteLegal(){return representanteLegal;}
    public void setRepresentanteLegal(String representanteLegal){this.representanteLegal = representanteLegal;}

    public String getRepresentanteCargo(){return representanteCargo;}
    public void setRepresentanteCargo(String representanteCargo){this.representanteCargo = representanteCargo;}

    public Integer getBolAtivo() {
        return bolAtivo;
    }

    public void setBolAtivo(Integer bolAtivo) {
        this.bolAtivo = bolAtivo;
    }

    public Set<Entidade> getEntidades() {
        return entidades;
    }
    public void setEntidades(Set<Entidade> entidades) {
        this.entidades = entidades;
    }

    public Set<Curso> getCursos() {
        return cursos;
    }
    public void setCursos(Set<Curso> cursos) {
        this.cursos = cursos;
    }

    public Set<Turma> getTurmas() {
        return turmas;
    }
    public void setTurmas(Set<Turma> turmas) {
        this.turmas = turmas;
    }

    public Set<Setor> getSetores() {
        return setores;
    }
    public void setSetores(Set<Setor> setores) {
        this.setores = setores;
    }

    public LocalDateTime getDataInclusao() {return dataInclusao; }
    public void setDataInclusao(LocalDateTime dataInclusao) {
        this.dataExclusao = dataInclusao;
    }

    public LocalDateTime getDataExclusao() {
        return dataExclusao;
    }
    public void setDataExclusao(LocalDateTime dataExclusao) {
        this.dataExclusao = dataExclusao;
    }


    @Override
    public String toString()
    {
        return "Campus [id=" + id + ", nome=" + nome + ", entidades=" + entidades +  ", setores=" + setores + ", cursos=" + cursos  + "]";
    }
}