package uft.edu.br.estagio.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Entity (name = "curso") //Define o nome da tabela que será criada no banco de dados
public class Curso implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "curso_seq", sequenceName = "curso_seq") //Cria uma sequence para ser usada com a tabela
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "curso_seq") //Define que a tabela fará uso da sequence criada antes
    private Long id;

    @Column(nullable = false, length = 250) //Define propriedades da coluna
    @NotBlank(message = "Infome um nome para o curso") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String nome;

    @Column(name = "bol_ativo")
    private Integer bolAtivo = 1;

    private String endereço;
    private String bairro;
    private String cidade;
    private String estado;
    private String cep;
    private String email;
    private String telefone;
    @Column(name = "representante_legal")
    private String representanteLegal;
    @Column(name = "representante_cargo")
    private String representanteCargo;

    @Column(name = "id_coordenador")
    private Long idCoordenador;

    @Column(name = "id_coordenador_substituto")
    private Long idCoordenadorSubstituto;

    @Column(name = "id_secretario_coordenacao")
    private Long idSecretarioCoordenacao;

    @Column(name = "data_inclusao")
    private LocalDateTime dataInclusao;

    @Column(name = "data_exclusao")
    private LocalDateTime dataExclusao;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "campus_curso",
            joinColumns = { @JoinColumn(name = "id_curso") },
            inverseJoinColumns = { @JoinColumn(name = "id_campus") })
    private Set<Campus> campi = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "turma_curso",
            joinColumns = { @JoinColumn(name = "id_curso") },
            inverseJoinColumns = { @JoinColumn(name = "id_turma") })
    private Set<Turma> turmas = new HashSet<>();


    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "cursos")

    private Set<Disciplina> disciplinas = new HashSet<>();
    
    public Curso()
    {
        super();
    }

    public Curso(Long id,
                 String nome,
                 String endereço,
                 String bairro,
                 String cidade,
                 String estado,
                 String cep,
                 String email,
                 String telefone,
                 String representanteLegal,
                 String representanteCargo,
                 Long idCoordenador,
                 Long idCoordenadorSubstituto,
                 Long idSecretarioCoordenacao,
                 LocalDateTime dataInclusao,
                 LocalDateTime dataExclusao,
                 Integer bolAtivo,
                 Set <Campus> campi,
                 Set <Disciplina> disciplinas
               )
    {
        super();
        this.id = id;
        this.nome = nome;
        this.endereço = endereço;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.cep = cep;
        this.email = email;
        this.telefone = telefone;
        this.representanteLegal = representanteLegal;
        this.representanteCargo = representanteCargo;
        this.idCoordenador = idCoordenador;
        this.idCoordenadorSubstituto = idCoordenadorSubstituto;
        this.idSecretarioCoordenacao = idSecretarioCoordenacao;
        this.dataInclusao = dataInclusao;
        this.dataExclusao = dataExclusao;
        this.bolAtivo = bolAtivo;
        this.campi = campi;
        this.disciplinas = disciplinas;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }


    public String getNome(){return nome;}
    public void setNome(String nome){this.nome = nome;}

    public String getEndereço(){return endereço;}
    public void setEndereço(String endereço){this.endereço = endereço;}

    public String getBairro(){return bairro;}
    public void setBairro(String bairro){this.bairro = bairro;}

    public String getCidade(){return cidade;}
    public void setCidade(String cidade){this.cidade = cidade;}

    public String getEstado(){return estado;}
    public void setEstado(String estado){this.estado = estado;}

    public String getCep(){return cep;}
    public void setCep(String cep){this.cep = cep;}

    public String getEmail(){return email;}
    public void setEmail(String email){this.email = email;}

    public String getTelefone(){return telefone;}
    public void setTelefone(String telefone){this.telefone = telefone;}

    public String getRepresentanteLegal(){return representanteLegal;}
    public void setRepresentanteLegal(String representanteLegal){this.representanteLegal = representanteLegal;}

    public String getRepresentanteCargo(){return representanteCargo;}
    public void setRepresentanteCargo(String representanteCargo){this.representanteCargo = representanteCargo;}

    public Long getIdCoordenador(){return idCoordenador;}
    public void setIdCoordenador(Long idCoordenador){this.idCoordenador = idCoordenador;}

    public Long getIdCoordenadorSubstituto(){return idCoordenadorSubstituto;}
    public void setIdCoordenadorSubstituto(Long idCoordenadorSubstituto){this.idCoordenadorSubstituto = idCoordenadorSubstituto;}



    public Long getIdSecretarioCoordenacao(){return idSecretarioCoordenacao;}
    public void setIdSecretarioCoordenacao(Long idSecretarioCoordenacao){this.idSecretarioCoordenacao = idSecretarioCoordenacao;}


    public LocalDateTime getDataInclusao() { return dataInclusao; }
    public void setDataInclusao(LocalDateTime dataInclusao) {
        this.dataExclusao = dataInclusao;
    }

    public LocalDateTime getDataExclusao() {
        return dataExclusao;
    }
    public void setDataExclusao(LocalDateTime dataExclusao) {
        this.dataExclusao = dataExclusao;
    }


    public Integer getBolAtivo(){return bolAtivo;}
    public void setBolAtivo(Integer bolAtivo){this.bolAtivo = bolAtivo;}

    //foreign keys


    public Set<Campus> getCampi() {
        return campi;
    }
    public void setCampi(Set<Campus> campi) {
        this.campi = campi;
    }

    public Set<Turma> getTurmas() {
        return turmas;
    }
    public void setTurmas(Set<Turma> turmas) {
        this.turmas = turmas;
    }

    public Set<Disciplina> getDisciplinas() {
        return disciplinas;
    }
    public void setDisciplinas(Set<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

}