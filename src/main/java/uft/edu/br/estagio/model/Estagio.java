package uft.edu.br.estagio.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Entity (name = "estagio") //Define o vaga da tabela que será criada no banco de dados
public class Estagio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "estagio_seq", sequenceName = "estagio_seq") //Cria uma sequence para ser usada com a tabela
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "estagio_seq") //Define que a tabela fará uso da sequence criada antes
    private Long id;

    @Column(nullable = false, length = 250) //Define propriedades da coluna
    @NotBlank(message = "Infome um vaga para o estagio") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String vaga;

    @Column(name = "bol_ativo")
    private Integer bolAtivo = 1;
    
    @Column(name = "data_inicio")
    private Date dataInicio;

    @Column(name = "data_fim")
    private Date dataFim;

    private String frequencia;

    @Column(name = "total_presenca")
    private Integer totalPresenca;

    @Column(name = "total_faltas")
    private Integer totalFaltas;

    private Float nota1;

    private Float nota2;

    private Float media;

    private Float exame;

    @Column(name = "media_final")
    private Integer mediaFinal;

    private String situacao;


    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })

    @JoinTable(name = "estagio_campus",
            joinColumns = { @JoinColumn(name = "id_estagio") },
            inverseJoinColumns = { @JoinColumn(name = "id_campus") })

    private Set<Campus> campi = new HashSet<>();



    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "estagio_departamento",
            joinColumns = { @JoinColumn(name = "id_estagio") },
            inverseJoinColumns = { @JoinColumn(name = "id_departamento") })

    private Set<Departamento> departamentos = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "estagio_aluno",
            joinColumns = { @JoinColumn(name = "id_estagio") },
            inverseJoinColumns = { @JoinColumn(name = "id_usuario") })
    private Set<Usuario> alunos = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "estagio_supervisor",
            joinColumns = { @JoinColumn(name = "id_estagio") },
            inverseJoinColumns = { @JoinColumn(name = "id_usuario") })
    private Set<Usuario> supervisores = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "estagio_plano",
            joinColumns = { @JoinColumn(name = "id_estagio") },
            inverseJoinColumns = { @JoinColumn(name = "id_plano") })
    private Set<Plano> planos = new HashSet<>();



/*
    @NotNull
    @NotEmpty
    private String password;
    private String matchingPassword;

    @NotNull
    @NotEmpty
    @ValidEmail
    @NotNull
    @NotEmpty
    private String email;
*/

/*
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "estagio_perfil",
            joinColumns = { @JoinColumn(name = "id_estagio") },
            inverseJoinColumns = { @JoinColumn(name = "id_perfil") })

    private Set<Perfil> perfis = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "estagio_curso",
            joinColumns = { @JoinColumn(name = "id_estagio") },
            inverseJoinColumns = { @JoinColumn(name = "id_curso") })
    private Set<Curso> cursos = new HashSet<>();
*/
    public Estagio()
    {
        super();
    }

    public Estagio(Long id,
                   String vaga,
                   Integer bolAtivo,
                   Date dataInicio,
                   Date dataFim,
                   String frequencia,
                   Integer totalPresenca,
                   Integer totalFaltas,
                   Float nota1,
                   Float nota2,
                   Float media,
                   Float exame,
                   Integer mediaFinal,
                   String situacao,
                   Set <Usuario> alunos,
                   Set <Usuario> supervisores,
                   Set <Plano> planos
                   //   Set<Curso> cursos
               )
    {
        super();
        this.id = id;
        this.vaga = vaga;
        this.vaga = vaga;
        this.bolAtivo = bolAtivo;
        this.vaga = vaga;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.frequencia = frequencia;
        this.totalPresenca = totalPresenca;
        this.totalFaltas = totalFaltas;
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.media = media;
        this.exame = exame;
        this.mediaFinal = mediaFinal;
        this.situacao = situacao;
        this.bolAtivo = bolAtivo;
        this.alunos = alunos;
        this.supervisores = supervisores;
        this.planos = planos;
       // this.cursos = cursos;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getVaga(){return vaga;}
    public void setVaga(String vaga){this.vaga = vaga;}

    public Date getDataInicio(){return dataInicio;}
    public void setDataInicio(Date dataInicio){this.dataInicio = dataInicio;}

    public Date getDataFim(){return dataFim;}
    public void setDataFim(Date dataFim){this.dataFim = dataFim;}

    public String getFrequencia(){return frequencia;}
    public void setFrequencia(String frequencia){this.frequencia = frequencia;}

    public Integer getTotalPresenca(){return totalPresenca;}
    public void setTotalPresenca(Integer totalPresenca){this.totalPresenca = totalPresenca;}

    public Integer getTotalFaltas(){return totalFaltas;}
    public void setTotalFaltas(Integer totalFaltas){this.totalFaltas = totalFaltas;}

    public Float getNota1(){return nota1;}
    public void setNota1(Float nota1){this.nota1 = nota1;}

    public Float getNota2(){return nota2;}
    public void setNota2(Float nota2){this.nota2 = nota2;}

    public Float getMedia(){return media;}
    public void setMedia(Float media){this.media = media;}

    public Float getExame(){return exame;}
    public void setExame(Float exame){this.exame = exame;}

    public Integer getMediaFinal(){return mediaFinal;}
    public void setMediaFinal(Integer mediaFinal){this.mediaFinal = mediaFinal;}

    public String getSituacao(){return situacao;}
    public void setSituacao(String situacao){this.situacao = situacao;}

    public Integer getBolAtivo(){return bolAtivo;}
    public void setBolAtivo(Integer bolAtivo){this.bolAtivo = bolAtivo;}



    //foreign keys

    public Set<Usuario> getAlunos() {
        return alunos;
    }
    public void setAlunos(Set<Usuario> alunos) {
        this.alunos = alunos;
    }

    public Set<Usuario> getSupervisores() {
        return supervisores;
    }
    public void setSupervisores(Set<Usuario> supervisores) {
        this.supervisores = supervisores;
    }

    public Set<Plano> getPlanos() {
        return planos;
    }
    public void setPlanos(Set<Plano> planos) {
        this.planos = planos;
    }



/*
    public Set<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(Set<Curso> cursos) {
        this.cursos = cursos;
    }

*/


}