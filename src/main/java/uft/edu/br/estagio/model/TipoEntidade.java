package uft.edu.br.estagio.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Entity (name = "tipo_entidade") //Define o nome da tabela que será criada no banco de dados
public class TipoEntidade implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "tipoEntidade_seq", sequenceName = "tipoEntidade_seq") //Cria uma sequence para ser usada com a tabela
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tipoEntidade_seq") //Define que a tabela fará uso da sequence criada antes
    private Long id;

    @Column(nullable = false, length = 150) //Define propriedades da coluna
    @NotBlank(message = "Infome um nome para o tipo Entidade") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String nome;
    

    @Column( length = 1, name = "bol_ativo") //Define propriedades da coluna
    private Integer bolAtivo = 1;

    @Column(name="data_inclusao",columnDefinition="TIMESTAMP")
    private LocalDateTime dataInclusao;

    @Column(name="data_exclusao",columnDefinition="TIMESTAMP")
    private LocalDateTime dataExclusao;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "tiposEntidade")

    private Set<Entidade> entidades = new HashSet<>();


    public TipoEntidade()
    {
        super();
    }

    public TipoEntidade(Long id, String nome, Set<Entidade> entidades)
    {
        super();
        this.id = id;
        this.nome = nome;
        this.dataInclusao = dataInclusao;
        this.dataExclusao = dataExclusao;
        this.entidades = entidades;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDateTime getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(LocalDateTime dataInclusao) {
        this.dataExclusao = dataInclusao;
    }

    public LocalDateTime getDataExclusao() {   return dataExclusao;}

    public void setDataExclusao(LocalDateTime dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public Integer getBolAtivo() {
        return bolAtivo;
    }

    public void setBolAtivo(Integer bolAtivo) {
        this.bolAtivo = bolAtivo;
    }

    public Set<Entidade> getEntidades() {
        return entidades;
    }

    public void setEntidades(Set<Entidade> entidades) {
        this.entidades = entidades;
    }


    @Override
    public String toString()
    {
        return "TipoEntidade [id=" + id + ", nome=" + nome + ", dataInclusao=" + dataInclusao + ", dataExclusao=" + dataExclusao + ", entidades=" + entidades + "]";
    }
}