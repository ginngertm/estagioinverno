package uft.edu.br.estagio.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Entity (name = "entidade") //Define o nome da tabela que será criada no banco de dados
public class Entidade implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "entidade_seq", sequenceName = "entidade_seq") //Cria uma sequence para ser usada com a tabela
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "entidade_seq") //Define que a tabela fará uso da sequence criada antes
    private Long id;

    @Column(nullable = false, length = 250) //Define propriedades da coluna
    @NotBlank(message = "Infome um nome para o entidade") //Define qual mensagem será exibida caso a validação da coluna falhar
    private String nome;

    @Column(name = "bol_ativo")
    private Integer bolAtivo = 1;

    @Column(name = "bol_adm_publica")
    private Integer bolAdmPublica;

    @Column(name = "razao_social")
    private String razaoSocial;
    private String cnpj;
    @Column(name = "inscricao_estadual")
    private String inscricaoEstadual;
    @Column(name = "ramo_atividade")
    private String ramoAtividade;
    private String endereço;
    private String bairro;
    private String cidade;
    private String estado;
    private String cep;
    private String email;
    private String telefone;
    @Column(name = "representante_legal")
    private String representanteLegal;
    @Column(name = "representante_cargo")
    private String representanteCargo;

    @Column(name = "data_inclusao")
    private LocalDateTime dataInclusao;

    @Column(name = "data_exclusao")
    private LocalDateTime dataExclusao;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "entidade_tipo",
            joinColumns = { @JoinColumn(name = "id_entidade") },
            inverseJoinColumns = { @JoinColumn(name = "id_tipo_entidade") })

    private Set<TipoEntidade> tiposEntidade = new HashSet<>();




    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })

    @JoinTable(name = "entidade_campus",
            joinColumns = { @JoinColumn(name = "id_entidade") },
            inverseJoinColumns = { @JoinColumn(name = "id_campus") })

    private Set<Campus> campi = new HashSet<>();


    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "entidades")

    private Set<Setor> setores = new HashSet<>();


    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })

    @JoinTable(name = "entidade_departamento",
            joinColumns = { @JoinColumn(name = "id_entidade") },
            inverseJoinColumns = { @JoinColumn(name = "id_departamento") })

    private Set<Departamento> departamentos = new HashSet<>();




/*
    @NotNull
    @NotEmpty
    private String password;
    private String matchingPassword;

    @NotNull
    @NotEmpty
    @ValidEmail
    @NotNull
    @NotEmpty
    private String email;
*/

/*
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "entidade_perfil",
            joinColumns = { @JoinColumn(name = "id_entidade") },
            inverseJoinColumns = { @JoinColumn(name = "id_perfil") })

    private Set<Perfil> perfis = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "entidade_curso",
            joinColumns = { @JoinColumn(name = "id_entidade") },
            inverseJoinColumns = { @JoinColumn(name = "id_curso") })
    private Set<Curso> cursos = new HashSet<>();
*/
    public Entidade()
    {
        super();
    }

    public Entidade(Long id,
                    String nome,
                    String razaoSocial,
                    String cnpj,
                    String inscricaoEstadual,
                    String ramoAtividade,
                    String endereço,
                    String bairro,
                    String cidade,
                    String estado,
                    String cep,
                    String email,
                    String telefone,
                    String representanteLegal,
                    String representanteCargo,
                    Integer bolAdmPublica,
                    Integer bolAtivo,
                    LocalDateTime dataInclusao,
                    LocalDateTime dataExclusao,
                    Set <TipoEntidade> tiposEntidade,
                    Set <Campus> campi,
                    Set <Setor> setores,
                    Set <Departamento> departamentos
                 //   Set<Curso> cursos
               )
    {
        super();
        this.id = id;
        this.nome = nome;
        this.razaoSocial = razaoSocial;
        this.cnpj = cnpj;
        this.inscricaoEstadual = inscricaoEstadual;
        this.ramoAtividade = ramoAtividade;
        this.endereço = endereço;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.cep = cep;
        this.email = email;
        this.telefone = telefone;
        this.representanteLegal = representanteLegal;
        this.representanteCargo = representanteCargo;
        this.bolAdmPublica = bolAdmPublica;
        this.bolAtivo = bolAtivo;
        this.dataInclusao = dataInclusao;
        this.dataExclusao = dataExclusao;
        this.tiposEntidade = tiposEntidade;
        this.campi = campi;
        this.setores = setores;
        this.departamentos = departamentos;
       // this.cursos = cursos;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }


    public String getNome(){return nome;}
    public void setNome(String nome){this.nome = nome;}

    public String getRazaoSocial(){return razaoSocial;}
    public void setRazaoSocial(String razaoSocial){this.razaoSocial = razaoSocial;}

    public String getCnpj(){return cnpj;}
    public void setCnpj(String cnpj){this.cnpj = cnpj;}

    public String getInscricaoEstadual(){return inscricaoEstadual;}
    public void setInscricaoEstadual(String inscricaoEstadual){this.inscricaoEstadual = inscricaoEstadual;}

    public String getRamoAtividade(){return ramoAtividade;}
    public void setRamoAtividade(String ramoAtividade){this.ramoAtividade = ramoAtividade;}

    public String getEndereço(){return endereço;}
    public void setEndereço(String endereço){this.endereço = endereço;}

    public String getBairro(){return bairro;}
    public void setBairro(String bairro){this.bairro = bairro;}

    public String getCidade(){return cidade;}
    public void setCidade(String cidade){this.cidade = cidade;}

    public String getEstado(){return estado;}
    public void setEstado(String estado){this.estado = estado;}

    public String getCep(){return cep;}
    public void setCep(String cep){this.cep = cep;}

    public String getEmail(){return email;}
    public void setEmail(String email){this.email = email;}

    public String getTelefone(){return telefone;}
    public void setTelefone(String telefone){this.telefone = telefone;}

    public String getRepresentanteLegal(){return representanteLegal;}
    public void setRepresentanteLegal(String representanteLegal){this.representanteLegal = representanteLegal;}

    public String getRepresentanteCargo(){return representanteCargo;}
    public void setRepresentanteCargo(String representanteCargo){this.representanteCargo = representanteCargo;}

    public Integer getBolAtivo(){return bolAtivo;}
    public void setBolAtivo(Integer bolAtivo){this.bolAtivo = bolAtivo;}

    public LocalDateTime getDataInclusao() { return dataInclusao; }
    public void setDataInclusao(LocalDateTime dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public LocalDateTime getDataExclusao() {
        return dataExclusao;
    }
    public void setDataExclusao(LocalDateTime dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public Integer getBolAdmPublica(){return bolAdmPublica;}
    public void setBolAdmPublica(Integer bolAdmPublica){this.bolAdmPublica = bolAdmPublica;}


    //foreign keys

    public Set<TipoEntidade> getTiposEntidade() {
        return tiposEntidade;
    }
    public void setTiposEntidade(Set<TipoEntidade> tiposEntidade) {
        this.tiposEntidade = tiposEntidade;
    }

    public Set<Campus> getCampi() {
        return campi;
    }
    public void setCampi(Set<Campus> campi) {
        this.campi = campi;
    }


    public Set<Setor> getSetores() {
        return setores;
    }
    public void setSetores(Set<Setor> setores) {
        this.setores = setores;
    }


    public Set<Departamento> getDepartamentos() {
        return departamentos;
    }
    public void setDepartamentos(Set<Departamento> departamentos) {
        this.departamentos = departamentos;
    }
/*
    public Set<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(Set<Curso> cursos) {
        this.cursos = cursos;
    }
*/


}