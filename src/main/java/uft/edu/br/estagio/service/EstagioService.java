package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.*;
import uft.edu.br.estagio.repository.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class EstagioService {

    @Autowired
    private EstagioRepository repository; //Injeta o repositório


    @Autowired
    private UsuarioRepository usuarioRepository; //Injeta o repositório

    @Autowired
    private CampusRepository campusRepository; //Injeta o repositório

    @Autowired
    private DepartamentoRepository departamentoRepository; //Injeta o repositório

    @Autowired
    private PlanoRepository planoRepository; //Injeta o repositório


    @Autowired
    private CursoRepository cursoRepository; //Injeta o repositório

    //Retorna uma lista com todos tiposEstagio inseridos
    public List<Estagio> findAll() {
        return repository.findAll();
    }

      /*
    //Retorna uma lista com todos tiposEstagio inseridos
    public List<Estagio> findByTipoEstagio(String tipoEstagio) {
         return (List<Estagio>) repository.findByTipoEstagio(tipoEstagio);
    }

    //Retorna uma lista com todos cursos inseridos
    public List<Estagio> findByCurso(String nome) {
        return (List<Estagio>) repository.findByCurso(nome);
    }

     */

    //Retorno um Estagio a partir do ID
    public Optional<Estagio> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um Estagio a partir do ID
    public Optional<Estagio> show(Long id) {
        return repository.findById(id);
    }



    //Salva ou atualiza um Estagio
    public Estagio save(Estagio estagio) {
        return repository.saveAndFlush(estagio);
    }

    //Exclui um Estagio
    public void delete(Long id) {
        repository.deleteById(id);
    }



    public List<Campus> getCampi(){
        return campusRepository.findAll();

    }

    public List<Departamento> getDepartamentos(){
        return departamentoRepository.findAll();

    }

    public List<Usuario> getAlunos(){
        return usuarioRepository.findAll();

    }

    public List<Usuario> getSupervisores(){
        return usuarioRepository.findAll();

    }

    public List<Plano> getPlanos(){
        return planoRepository.findAll();

    }



    /*
    public List<Curso> getCursos(){
        return cursoRepository.findAll();
    }

     */

}