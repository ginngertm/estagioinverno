package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.*;
import uft.edu.br.estagio.repository.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class TurmaService {

    @Autowired
    private TurmaRepository repository; //Injeta o repositório


    @Autowired
    private UsuarioRepository usuarioRepository; //Injeta o repositório


    @Autowired
    private CursoRepository cursoRepository; //Injeta o repositório

    @Autowired
    private CampusRepository campusRepository; //Injeta o repositório

    @Autowired
    private DisciplinaRepository disciplinaRepository; //Injeta o repositório

    //Retorna uma lista com todos tiposTurma inseridos
    public List<Turma> findAll() {
        return repository.findAll();
    }

    //Retorna uma lista com todos tiposTurma inseridos
    public List<Usuario> findByPerfil(String perfil) {
        return usuarioRepository.findByPerfil(perfil);
    }


      /*
    //Retorna uma lista com todos tiposTurma inseridos
    public List<Turma> findByTipoTurma(String tipoTurma) {
         return (List<Turma>) repository.findByTipoTurma(tipoTurma);
    }

    //Retorna uma lista com todos cursos inseridos
    public List<Turma> findByCurso(String nome) {
        return (List<Turma>) repository.findByCurso(nome);
    }

     */

    //Retorno um Turma a partir do ID
    public Optional<Turma> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um Turma a partir do ID
    public Optional<Turma> show(Long id) {
        return repository.findById(id);
    }



    //Salva ou atualiza um Turma
    public Turma save(Turma turma) {
        return repository.saveAndFlush(turma);
    }

    //Exclui um Turma
    public void delete(Long id) {
        repository.deleteById(id);
    }


    public List<Usuario> getUsuarios(){return usuarioRepository.findAll(); }
    public List<Campus> getCampi(){return campusRepository.findAll(); }
    public List<Curso> getCursos(){return cursoRepository.findAll(); }
    public List<Disciplina> getDisciplinas(){return disciplinaRepository.findAll(); }





}