package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.Campus;
import uft.edu.br.estagio.model.Curso;
import uft.edu.br.estagio.model.Disciplina;
import uft.edu.br.estagio.model.Turma;
import uft.edu.br.estagio.repository.CampusRepository;
import uft.edu.br.estagio.repository.CursoRepository;
import uft.edu.br.estagio.repository.DisciplinaRepository;
import uft.edu.br.estagio.repository.TurmaRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class CursoService {

    @Autowired
    private CursoRepository repository; //Injeta o repositório

    @Autowired
    private CampusRepository campusRepository; //Injeta o repositório

    @Autowired
    private TurmaRepository turmaRepository; //Injeta o repositório

    @Autowired
    private DisciplinaRepository disciplinaRepository; //Injeta o repositório

    //Retorna uma lista com todos tiposCurso inseridos
    public List<Curso> findAll() {
        return repository.findAll();
    }

      /*
    //Retorna uma lista com todos tiposCurso inseridos
    public List<Curso> findByTipoCurso(String tipoCurso) {
         return (List<Curso>) repository.findByTipoCurso(tipoCurso);
    }

    //Retorna uma lista com todos cursos inseridos
    public List<Curso> findByCurso(String nome) {
        return (List<Curso>) repository.findByCurso(nome);
    }

     */

    //Retorno um Curso a partir do ID
    public Optional<Curso> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um Curso a partir do ID
    public Optional<Curso> show(Long id) {
        return repository.findById(id);
    }



    //Salva ou atualiza um Curso
    public Curso save(Curso curso) {
        return repository.saveAndFlush(curso);
    }

    //Exclui um Curso
    public void delete(Long id) {
        repository.deleteById(id);
    }


    public List<Campus> getCampi(){ return campusRepository.findAll(); }

    public List<Turma> getTurmas(){ return turmaRepository.findAll(); }

    public List<Disciplina> getDisciplinas(){ return disciplinaRepository.findAll(); }






}