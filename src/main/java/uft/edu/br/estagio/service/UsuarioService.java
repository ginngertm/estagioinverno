package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.*;
import uft.edu.br.estagio.repository.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository; //Injeta o repositório


    @Autowired
    private PerfilRepository perfilRepository; //Injeta o repositório

    @Autowired
    private CursoRepository cursoRepository; //Injeta o repositório

    @Autowired
    private TurmaRepository turmaRepository; //Injeta o repositório

    @Autowired
    private SetorRepository setorRepository; //Injeta o repositório

    //Retorna uma lista com todos perfis inseridos
    public List<Usuario> findAll() {
        return repository.findAll();
    }

    //Retorna uma lista com todos cursos inseridos
    public List<Usuario> findByCurso(String nome) {
        return (List<Usuario>) repository.findByCurso(nome);
    }

    //Retorno um Usuario a partir do ID
    public Optional<Usuario> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um Usuario a partir do ID
    public Optional<Usuario> show(Long id) {
        return repository.findById(id);
    }



    //Salva ou atualiza um Usuario
    public Usuario save(Usuario usuario) {
        return repository.saveAndFlush(usuario);
    }

    //Exclui um Usuario
    public void delete(Long id) {
        repository.deleteById(id);
    }

    public List<Perfil> getPerfis(){
        return perfilRepository.findAll();

    }

    public List<Curso> getCursos(){
        return cursoRepository.findAll();
    }

    public List<Turma> getTurmas(){
        return turmaRepository.findAll();
    }

    public List<Setor> getSetores(){
        return setorRepository.findAll();
    }


    //Retorna uma lista com todos perfis inseridos
    public List<Usuario> findByPerfil(String perfil) {
        return (List<Usuario>) repository.findByPerfil(perfil);
    }

    /*
    @org.springframework.transaction.annotation.Transactional
    @Override
    public User registerNewUserAccount(UserDto accountDto)
            throws EmailExistsException {

        if (emailExist(accountDto.getEmail())) {
            throw new EmailExistsException(
                    "There is an account with that email adress: "
                            +  accountDto.getEmail());
        }

        // the rest of the registration operation
    }
    private boolean emailExist(String email) {
        User user = repository.findByEmail(email);
        if (user != null) {
            return true;
        }
        return false;
    }
    */

}