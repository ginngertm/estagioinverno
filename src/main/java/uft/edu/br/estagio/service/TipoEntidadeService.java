package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.TipoEntidade;
import uft.edu.br.estagio.repository.TipoEntidadeRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class TipoEntidadeService {

    @Autowired
    private TipoEntidadeRepository repository; //Injeta o repositório

    //Retorna uma lista com todos tiposEntidades inseridos
    public List<TipoEntidade> findAll() {
        return repository.findAll();
    }

    //Retorno um TipoEntidade a partir do ID
    public Optional<TipoEntidade> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um TipoEntidade a partir do ID
    public Optional<TipoEntidade> show(Long id) {
        return repository.findById(id);
    }

    //Salva ou atualiza um TipoEntidade
    public TipoEntidade save(TipoEntidade tipoEntidade) {
        return repository.saveAndFlush(tipoEntidade);
    }

    //Exclui um TipoEntidade
    // public void delete(Long id) { repository.deleteById(id); }
     public void delete(Long id) { repository.setSinAtivoFalse(id); }



}