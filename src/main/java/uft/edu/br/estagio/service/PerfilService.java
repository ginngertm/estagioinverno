package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.Perfil;
import uft.edu.br.estagio.model.Permissao;
import uft.edu.br.estagio.repository.PerfilRepository;
import uft.edu.br.estagio.repository.PermissaoRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class PerfilService {

    @Autowired
    private PerfilRepository repository; //Injeta o repositório

    @Autowired
    private PermissaoRepository permissaoRepository; //Injeta o repositório

    //Retorna uma lista com todos perfis inseridos
    public List<Perfil> findAll() {
        return repository.findAll();
    }

    //Retorno um Perfil a partir do ID
    public Optional<Perfil> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um Perfil a partir do ID
    public Optional<Perfil> show(Long id) {
        return repository.findById(id);
    }

    //Salva ou atualiza um Perfil
    public Perfil save(Perfil perfil) {
        return repository.saveAndFlush(perfil);
    }

    //Exclui um Perfil
    public void delete(Long id) {
        repository.deleteById(id);
    }

    public List<Permissao> getPermissoes(){
        return permissaoRepository.findAll();
    }




}