package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.*;
import uft.edu.br.estagio.repository.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class CampusService {

    @Autowired
    private CampusRepository repository; //Injeta o repositório

    @Autowired
    private EntidadeRepository entidadeRepository; //Injeta o repositório

    @Autowired
    private CursoRepository cursoRepository; //Injeta o repositório

    @Autowired
    private TurmaRepository turmaRepository; //Injeta o repositório

    @Autowired
    private SetorRepository setorRepository; //Injeta o repositório


    //Retorna uma lista com todos campi inseridos
    public List<Campus> findAll() {
        return repository.findAll();
    }

    //Retorno um Campus a partir do ID
    public Optional<Campus> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um Campus a partir do ID
    public Optional<Campus> show(Long id) {
        return repository.findById(id);
    }

    //Salva ou atualiza um Campus
    public Campus save(Campus campus) {
        return repository.saveAndFlush(campus);
    }

    //Exclui um Campus
    public void delete(Long id) {
        repository.deleteById(id);
    }

    public List<Entidade> getEntidades(){
        return entidadeRepository.findAll();
    }

    public List<Curso> getCursos(){
        return cursoRepository.findAll();
    }

    public List<Turma> getTurmas(){ return turmaRepository.findAll(); }

    public List<Setor> getSetores(){ return setorRepository.findAll(); }



}