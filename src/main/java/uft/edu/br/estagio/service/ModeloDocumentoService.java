package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.ModeloDocumento;
import uft.edu.br.estagio.repository.ModeloDocumentoRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class ModeloDocumentoService {

    @Autowired
    private ModeloDocumentoRepository repository; //Injeta o repositório

    //Retorna uma lista com todos modeloDocumentos inseridos
    public List<ModeloDocumento> findAll() {
        return repository.findAll();
    }

    //Retorno um ModeloDocumento a partir do ID
    public Optional<ModeloDocumento> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um ModeloDocumento a partir do ID
    public Optional<ModeloDocumento> show(Long id) {
        return repository.findById(id);
    }

    //Salva ou atualiza um ModeloDocumento
    public ModeloDocumento save(ModeloDocumento modeloDocumento) {
        return repository.saveAndFlush(modeloDocumento);
    }

    //Exclui um ModeloDocumento
    public void delete(Long id) {
        repository.deleteById(id);
    }



}