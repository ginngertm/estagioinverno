package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.Departamento;
import uft.edu.br.estagio.model.Entidade;
import uft.edu.br.estagio.repository.DepartamentoRepository;
import uft.edu.br.estagio.repository.EntidadeRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class DepartamentoService {

  
    @Autowired
    private DepartamentoRepository repository; //Injeta o repositório

    @Autowired
    private EntidadeRepository entidadeRepository; //Injeta o repositório
    


    //Retorna uma lista com todos departamentos inseridos
    public List<Departamento> findAll() {
        return repository.findAll();
    }

    //Retorno um Departamento a partir do ID
    public Optional<Departamento> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um Departamento a partir do ID
    public Optional<Departamento> show(Long id) {
        return repository.findById(id);
    }

    //Salva ou atualiza um Departamento
    public Departamento save(Departamento departamento) {
        return repository.saveAndFlush(departamento);
    }

    //Exclui um Departamento
    public void delete(Long id) {
        repository.deleteById(id);
    }

    public List<Entidade> getEntidades(){
        return entidadeRepository.findAll();
    }




}