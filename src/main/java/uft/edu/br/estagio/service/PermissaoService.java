package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.Perfil;
import uft.edu.br.estagio.model.Permissao;
import uft.edu.br.estagio.repository.PerfilRepository;
import uft.edu.br.estagio.repository.PermissaoRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class PermissaoService {

    @Autowired
    private PermissaoRepository repository; //Injeta o repositório

    @Autowired
    private PerfilRepository perfilRepository; //Injeta o repositório

    //Retorna uma lista com todos permissoes inseridos
    public List<Permissao> findAll() {
        return repository.findAll();
    }

    public List<Perfil> getPerfis(){
        return perfilRepository.findAll();
    }


    //Retorno um Permissao a partir do ID
    public Optional<Permissao> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um Permissao a partir do ID
    public Optional<Permissao> show(Long id) {
        return repository.findById(id);
    }

    //Salva ou atualiza um Permissao
    public Permissao save(Permissao permissao) {
        return repository.saveAndFlush(permissao);
    }

    //Exclui um Permissao
    public void delete(Long id) {
        repository.deleteById(id);
    }



}