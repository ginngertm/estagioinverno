package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.Curso;
import uft.edu.br.estagio.model.Disciplina;
import uft.edu.br.estagio.model.Turma;
import uft.edu.br.estagio.repository.CampusRepository;
import uft.edu.br.estagio.repository.CursoRepository;
import uft.edu.br.estagio.repository.DisciplinaRepository;
import uft.edu.br.estagio.repository.TurmaRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class DisciplinaService {

    @Autowired
    private DisciplinaRepository repository; //Injeta o repositório

    @Autowired
    private CursoRepository cursoRepository; //Injeta o repositório

    @Autowired
    private CampusRepository campusRepository; //Injeta o repositório

    @Autowired
    private TurmaRepository turmaRepository; //Injeta o repositório

    //Retorna uma lista com todos tiposCursos inseridos
    public List<Disciplina> findAll() {
        return repository.findAll();
    }

    //Retorno um Disciplina a partir do ID
    public Optional<Disciplina> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um Disciplina a partir do ID
    public Optional<Disciplina> show(Long id) {
        return repository.findById(id);
    }

    //Salva ou atualiza um Disciplina
    public Disciplina save(Disciplina disciplina) {
        return repository.saveAndFlush(disciplina);
    }

    //Exclui um Disciplina
    // public void delete(Long id) { repository.deleteById(id); }
     public void delete(Long id) { repository.setSinAtivoFalse(id); }

    public List<Curso> getCursos(){
        return cursoRepository.findAll();

    }

    public List<Turma> getTurmas(){
        return turmaRepository.findAll();

    }





}