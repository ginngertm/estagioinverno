package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.*;
import uft.edu.br.estagio.repository.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class EntidadeService {

    @Autowired
    private EntidadeRepository repository; //Injeta o repositório


    @Autowired
    private TipoEntidadeRepository tipoEntidadeRepository; //Injeta o repositório

    @Autowired
    private CampusRepository campusRepository; //Injeta o repositório

    @Autowired
    private SetorRepository setorRepository; //Injeta o repositório


    @Autowired
    private DepartamentoRepository departamentoRepository; //Injeta o repositório


    @Autowired
    private CursoRepository cursoRepository; //Injeta o repositório

    //Retorna uma lista com todos tiposEntidade inseridos
    public List<Entidade> findAll() {
        return repository.findAll();
    }

      /*
    //Retorna uma lista com todos tiposEntidade inseridos
    public List<Entidade> findByTipoEntidade(String tipoEntidade) {
         return (List<Entidade>) repository.findByTipoEntidade(tipoEntidade);
    }

    //Retorna uma lista com todos cursos inseridos
    public List<Entidade> findByCurso(String nome) {
        return (List<Entidade>) repository.findByCurso(nome);
    }

     */

    //Retorno um Entidade a partir do ID
    public Optional<Entidade> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um Entidade a partir do ID
    public Optional<Entidade> show(Long id) {
        return repository.findById(id);
    }



    //Salva ou atualiza um Entidade
    public Entidade save(Entidade entidade) {
        return repository.saveAndFlush(entidade);
    }

    //Exclui um Entidade
    public void delete(Long id) {
        repository.deleteById(id);
    }


    public List<TipoEntidade> getTiposEntidade(){
        return tipoEntidadeRepository.findAll();

    }

    public List<Campus> getCampi(){
        return campusRepository.findAll();

    }

    public List<Setor> getSetores(){
        return setorRepository.findAll();

    }

    public List<Departamento> getDepartamentos(){
        return departamentoRepository.findAll();

    }




    /*
    public List<Curso> getCursos(){
        return cursoRepository.findAll();
    }

     */

}