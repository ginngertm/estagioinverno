package uft.edu.br.estagio.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uft.edu.br.estagio.model.Estagio;
import uft.edu.br.estagio.model.Plano;
import uft.edu.br.estagio.repository.EstagioRepository;
import uft.edu.br.estagio.repository.PlanoRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

//import java.util.Optional;
//import com.sun.mail.imap.protocol.ID;

@Service //Define a classe como um bean do Spring
@Transactional
public class PlanoService {

    @Autowired
    private PlanoRepository repository; //Injeta o repositório


    @Autowired
    private EstagioRepository estagioRepository; //Injeta o repositório
    

    //Retorna uma lista com todos planos inseridos
    public List<Plano> findAll() {
        return repository.findAll();
    }

    //Retorno um Plano a partir do ID
    public Optional<Plano> findOne(Long id) {
        return repository.findById(id);
    }

    //Exibe um Plano a partir do ID
    public Optional<Plano> show(Long id) {
        return repository.findById(id);
    }

    //Salva ou atualiza um Plano
    public Plano save(Plano plano) {
        return repository.saveAndFlush(plano);
    }

    //Exclui um Plano
    public void delete(Long id) {
        repository.deleteById(id);
    }

    public List<Estagio> getEstagios(){
        return estagioRepository.findAll();
    }

    public List<Plano> getPlanos(){
        return repository.findAll();
    }





}