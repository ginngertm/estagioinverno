package uft.edu.br.estagio;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/*
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import uft.edu.br.estagio.Usuario.UsuarioRepository;
//

 */

@SpringBootApplication
@EnableJpaRepositories(basePackages= "uft.edu.br.estagio.repository")
@EnableTransactionManagement
@EntityScan(basePackages="uft.edu.br.estagio.model")
// @ComponentScan(basePackages={"uft.edu.br.estagio"})
@RestController


public class EstagioApplication{

	@RequestMapping(value = "/index",method = RequestMethod.GET)
	public ModelAndView index(ModelAndView modelAndView) {
		modelAndView = new ModelAndView("index");
		System.out.println("entrou no /index");
		return modelAndView;
	}

	@RequestMapping(value= "/", method = RequestMethod.GET)
	public ModelAndView home(ModelAndView modelAndView) {
		modelAndView = new ModelAndView("index");
		System.out.println("entrou no /");
		return modelAndView;
	}

	public static void main(String[] args) {
		SpringApplication.run(EstagioApplication.class, args);

	}

}
