package uft.edu.br.estagio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import uft.edu.br.estagio.model.Departamento;
import uft.edu.br.estagio.service.DepartamentoService;

import javax.validation.Valid;


@Controller
public class DepartamentoController {


    @Autowired
    private DepartamentoService service;


    @GetMapping("/Departamento/list")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("/Departamento/list");
        mv.addObject("departamentos", service.findAll());
        mv.addObject("entidades", service.getEntidades());

        return mv;
    }

    @GetMapping("/Departamento/add")
    public ModelAndView add(Departamento departamento) {
        ModelAndView mv = new ModelAndView("/Departamento/edit");
        mv.addObject("departamento", departamento);
        mv.addObject("entidades", service.getEntidades());


        return mv;
    }

    @GetMapping("/Departamento/show/{id}")
    public ModelAndView show(@PathVariable("id") Long id) {
        Departamento departamento = service.show(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        ModelAndView mv = new ModelAndView("/Departamento/show");
        mv.addObject("departamento", departamento);
        return mv;

    }

    @GetMapping("/Departamento/edit/{id}")
    public ModelAndView update(@PathVariable("id") Long id) {
        Departamento departamento = service.findOne(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
            return add(departamento);
    }

    @GetMapping("/Departamento/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        service.delete(id);
        return findAll();
    }

    @PostMapping("/Departamento/save")
    public ModelAndView save(@RequestParam("entidades") String entidades, @Valid Departamento departamento, BindingResult result) {
        if(result.hasErrors()) {
            return add(departamento);
        }
        service.save(departamento);
        return findAll();
    }
}