package uft.edu.br.estagio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import uft.edu.br.estagio.model.Perfil;
import uft.edu.br.estagio.service.PerfilService;

import javax.validation.Valid;


@Controller
public class PerfilController {


    @Autowired
    private PerfilService service;

    @GetMapping("/Perfil/list")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("/Perfil/list");
        mv.addObject("perfis", service.findAll());
        mv.addObject("permissoes", service.getPermissoes());
        return mv;
    }

    @GetMapping("/Perfil/add")
    public ModelAndView add(Perfil perfil) {
        ModelAndView mv = new ModelAndView("/Perfil/edit");
        mv.addObject("perfil", perfil);
        mv.addObject("permissoes", service.getPermissoes());
        return mv;
    }

    @GetMapping("/Perfil/show/{id}")
    public ModelAndView show(@PathVariable("id") Long id) {
        Perfil perfil = service.show(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        ModelAndView mv = new ModelAndView("/Perfil/show");
        mv.addObject("perfil", perfil);
        mv.addObject("permissoes", service.getPermissoes());

        return mv;

    }

    @GetMapping("/Perfil/edit/{id}")
    public ModelAndView update(@PathVariable("id") Long id) {
        Perfil perfil = service.findOne(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
            return add(perfil);
    }

    @GetMapping("/Perfil/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        service.delete(id);
        return findAll();
    }

    @PostMapping("/Perfil/save")
    public ModelAndView save(@Valid Perfil perfil,  BindingResult result) {
        if(result.hasErrors()) {
            return add(perfil);
        }
        service.save(perfil);
        return findAll();
    }

}