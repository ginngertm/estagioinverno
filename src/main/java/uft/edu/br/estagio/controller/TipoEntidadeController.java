package uft.edu.br.estagio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import uft.edu.br.estagio.model.TipoEntidade;
import uft.edu.br.estagio.service.TipoEntidadeService;

import javax.validation.Valid;


@Controller
public class TipoEntidadeController {


    @Autowired
    private TipoEntidadeService service;

    @GetMapping("/TipoEntidade/list")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("/TipoEntidade/list");
        mv.addObject("tiposEntidade", service.findAll());
        return mv;
    }

    @GetMapping("/TipoEntidade/add")
    public ModelAndView add(TipoEntidade tipoEntidade) {
        ModelAndView mv = new ModelAndView("/TipoEntidade/edit");
        mv.addObject("tipoEntidade", tipoEntidade);
        return mv;
    }

    @GetMapping("/TipoEntidade/show/{id}")
    public ModelAndView show(@PathVariable("id") Long id) {
        TipoEntidade tipoEntidade = service.show(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        ModelAndView mv = new ModelAndView("/TipoEntidade/show");
        mv.addObject("tipoEntidade", tipoEntidade);
        return mv;

    }

    @GetMapping("/TipoEntidade/edit/{id}")
    public ModelAndView update(@PathVariable("id") Long id) {
        TipoEntidade tipoEntidade = service.findOne(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
            return add(tipoEntidade);
    }

    @GetMapping("/TipoEntidade/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        service.delete(id);
        return findAll();
    }

    @PostMapping("/TipoEntidade/save")
    public ModelAndView save(@Valid TipoEntidade tipoEntidade, BindingResult result) {
        if(result.hasErrors()) {
            return add(tipoEntidade);
        }
        service.save(tipoEntidade);
        return findAll();
    }

}