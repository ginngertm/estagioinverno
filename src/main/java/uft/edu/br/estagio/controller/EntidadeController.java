package uft.edu.br.estagio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import uft.edu.br.estagio.model.Entidade;
import uft.edu.br.estagio.model.TipoEntidade;
import uft.edu.br.estagio.service.EntidadeService;

import javax.validation.Valid;


@Controller
public class EntidadeController {

    @Autowired
    private EntidadeService service;

/*
    @Autowired
    private TipoEntidadeService tipoEntidadeService;
    private Map<Long, TipoEntidade> tiposEntidade = new HashMap<>();

    @Autowired
    private CursoService cursoService;
    private Map<Long, Curso> cursos = new HashMap<>();
    
     */

    @GetMapping("/Entidade/list")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("/Entidade/list");
        mv.addObject("entidades", service.findAll());
        mv.addObject("tiposEntidade", service.getTiposEntidade());
        mv.addObject("campi", service.getCampi());
        mv.addObject("setores", service.getSetores());
        mv.addObject("departamentos", service.getDepartamentos());
      //  mv.addObject("cursos", service.getCursos());
        return mv;
    }

    /*
    @RequestMapping(path="/Entidade/findBy/perfil={perfil}" , method = RequestMethod.GET)
    public ModelAndView findAllByTipoEntidade(@PathVariable("perfil") String perfil) {
        ModelAndView mv = new ModelAndView("/Entidade/list");
        mv.addObject("entidades", service.findByTipoEntidade(perfil));
     //   mv.addObject("tiposEntidade", service.getTiposEntidade());
     //   mv.addObject("cursos", service.getCursos());
        return mv;
    }

     */

    @GetMapping("/Entidade/add")
    public ModelAndView add(Entidade entidade) {
        ModelAndView mv = new ModelAndView("/Entidade/edit");
        TipoEntidade p = new TipoEntidade();
        mv.addObject("entidade", entidade);
        mv.addObject("tiposEntidade", service.getTiposEntidade());
        mv.addObject("campi", service.getCampi());
        mv.addObject("setores", service.getSetores());
        mv.addObject("departamentos", service.getDepartamentos());
     //   mv.addObject("cursos", service.getCursos());
        return mv;
    }

    @GetMapping("/Entidade/show/{id}")
    public ModelAndView show(@PathVariable("id") Long id) {
        Entidade entidade = service.show(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid entidade Id:" + id));
        ModelAndView mv = new ModelAndView("/Entidade/show");
        mv.addObject("entidade", entidade);
        return mv;

    }

    @GetMapping("/Entidade/edit/{id}")
    public ModelAndView update(@PathVariable("id") Long id) {
        Entidade entidade = service.findOne(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid entidade Id:" + id));
            return add(entidade);
    }

    @GetMapping("/Entidade/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        service.delete(id);
        return findAll();
    }

    @PostMapping("/Entidade/save")
   // public ModelAndView save(@RequestParam("tiposEntidade") String tiposEntidade, @RequestParam("cursos") String cursos, @Valid Entidade entidade, BindingResult result) {
    public ModelAndView save(@RequestParam("tiposEntidade") String tiposEntidade,   @Valid Entidade entidade, BindingResult result) {
        if(result.hasErrors()) {
            return add(entidade);
        }
        service.save(entidade);
        return findAll();
    }

}