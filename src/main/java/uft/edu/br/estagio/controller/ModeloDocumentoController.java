package uft.edu.br.estagio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import uft.edu.br.estagio.model.ModeloDocumento;
import uft.edu.br.estagio.service.ModeloDocumentoService;

import javax.validation.Valid;


@Controller
public class ModeloDocumentoController {


    @Autowired
    private ModeloDocumentoService service;

    @GetMapping("/ModeloDocumento/list")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("/ModeloDocumento/list");
        mv.addObject("modeloDocumentos", service.findAll());
        return mv;
    }

    @GetMapping("/ModeloDocumento/add")
    public ModelAndView add(ModeloDocumento modeloDocumento) {
        ModelAndView mv = new ModelAndView("/ModeloDocumento/edit");
        mv.addObject("modeloDocumento", modeloDocumento);
        return mv;
    }

    @GetMapping("/ModeloDocumento/show/{id}")
    public ModelAndView show(@PathVariable("id") Long id) {
        ModeloDocumento modeloDocumento = service.show(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        ModelAndView mv = new ModelAndView("/ModeloDocumento/show");
        mv.addObject("modeloDocumento", modeloDocumento);
        return mv;

    }

    @GetMapping("/ModeloDocumento/edit/{id}")
    public ModelAndView update(@PathVariable("id") Long id) {
        ModeloDocumento modeloDocumento = service.findOne(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
            return add(modeloDocumento);
    }

    @GetMapping("/ModeloDocumento/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        service.delete(id);
        return findAll();
    }

    @PostMapping("/ModeloDocumento/save")
    public ModelAndView save(@Valid ModeloDocumento modeloDocumento, BindingResult result) {
        if(result.hasErrors()) {
            return add(modeloDocumento);
        }
        service.save(modeloDocumento);
        return findAll();
    }

}