package uft.edu.br.estagio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import uft.edu.br.estagio.model.Plano;
import uft.edu.br.estagio.service.PlanoService;

import javax.validation.Valid;


@Controller
public class PlanoController {


    @Autowired
    private PlanoService service;


    @GetMapping("/Plano/list")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("/Plano/list");
        mv.addObject("planos", service.findAll());
        mv.addObject("estagios", service.getEstagios());
        return mv;
    }

    @GetMapping("/Plano/add")
    public ModelAndView add(Plano plano) {
        ModelAndView mv = new ModelAndView("/Plano/edit");
        mv.addObject("plano", plano);
        mv.addObject("estagios", service.getEstagios());
        return mv;
    }

    @GetMapping("/Plano/show/{id}")
    public ModelAndView show(@PathVariable("id") Long id) {
        Plano plano = service.show(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        ModelAndView mv = new ModelAndView("/Plano/show");
        mv.addObject("plano", plano);
        mv.addObject("estagios", service.getEstagios());

        return mv;

    }

    @GetMapping("/Plano/edit/{id}")
    public ModelAndView update(@PathVariable("id") Long id) {
        Plano plano = service.findOne(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
            return add(plano);
    }

    @GetMapping("/Plano/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        service.delete(id);
        return findAll();
    }

    @PostMapping("/Plano/save")
    public ModelAndView save( @Valid Plano plano,  BindingResult result) {
        if(result.hasErrors()) {
            return add(plano);
        }
        service.save(plano);
        return findAll();
    }

}