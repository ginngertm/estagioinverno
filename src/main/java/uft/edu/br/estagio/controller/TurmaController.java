package uft.edu.br.estagio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import uft.edu.br.estagio.model.Turma;
import uft.edu.br.estagio.model.Usuario;
import uft.edu.br.estagio.service.TurmaService;
import uft.edu.br.estagio.service.UsuarioService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;


@Controller
public class TurmaController {

    @Autowired
    private TurmaService service;

    @Autowired
    private UsuarioService usuarioService;
    private Map<Long, Usuario> alunos = new HashMap<>();

/*
    @Autowired
    private UsuarioService tipoTurmaService;
    private Map<Long, Usuario> tiposTurma = new HashMap<>();

    @Autowired
    private CursoService cursoService;
    private Map<Long, Curso> cursos = new HashMap<>();
    
     */

    @GetMapping("/Turma/list")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("/Turma/list");
        mv.addObject("turmas", service.findAll());
        mv.addObject("alunos", service.findByPerfil("Aluno"));
        mv.addObject("docentes", service.findByPerfil("Supervisor"));
        mv.addObject("cursos", service.getCursos());
        mv.addObject("campi", service.getCampi());
        mv.addObject("disciplinas", service.getDisciplinas());

      //  mv.addObject("campi", service.getCampi());
      //  mv.addObject("cursos", service.getCursos());
        return mv;
    }


    @GetMapping("/Turma/add")
    public ModelAndView add(Turma turma) {
        ModelAndView mv = new ModelAndView("/Turma/edit");
        Usuario p = new Usuario();
        mv.addObject("turma", turma);
        mv.addObject("alunos", service.findByPerfil("Aluno"));
        mv.addObject("docentes", service.findByPerfil("Supervisor"));
        mv.addObject("cursos", service.getCursos());
        mv.addObject("campi", service.getCampi());
        mv.addObject("disciplinas", service.getDisciplinas());

        //   mv.addObject("cursos", service.getCursos());
        return mv;
    }

    @GetMapping("/Turma/show/{id}")
    public ModelAndView show(@PathVariable("id") Long id) {
        Turma turma = service.show(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid turma Id:" + id));
        ModelAndView mv = new ModelAndView("/Turma/show");
        mv.addObject("turma", turma);
        return mv;

    }

    @GetMapping("/Turma/edit/{id}")
    public ModelAndView update(@PathVariable("id") Long id) {
        Turma turma = service.findOne(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid turma Id:" + id));
            return add(turma);
    }

    @GetMapping("/Turma/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        service.delete(id);
        return findAll();
    }

    @PostMapping("/Turma/save")
   // public ModelAndView save(@RequestParam("tiposTurma") String tiposTurma, @RequestParam("cursos") String cursos, @Valid Turma turma, BindingResult result) {
    public ModelAndView save( @RequestParam("disciplinas") String disciplinas, @RequestParam("cursos") String cursos,  @RequestParam("campi") String campi, @Valid Turma turma, BindingResult result) {
        if(result.hasErrors()) {
            return add(turma);
        }
        service.save(turma);
        return findAll();
    }

}