package uft.edu.br.estagio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uft.edu.br.estagio.model.Curso;
import uft.edu.br.estagio.model.Perfil;
import uft.edu.br.estagio.model.Usuario;
import uft.edu.br.estagio.service.CursoService;
import uft.edu.br.estagio.service.PerfilService;
import uft.edu.br.estagio.service.UsuarioService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;


@Controller
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @Autowired
    private PerfilService perfilService;
    private Map<Long, Perfil> perfis = new HashMap<>();

    @Autowired
    private CursoService cursoService;
    private Map<Long, Curso> cursos = new HashMap<>();

    @GetMapping("/Usuario/list")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("/Usuario/list");
        mv.addObject("usuarios", service.findAll());
        mv.addObject("perfis", service.getPerfis());
        mv.addObject("cursos", service.getCursos());
        mv.addObject("turmas", service.getTurmas());
        return mv;
    }

    @RequestMapping(path="/Usuario/findBy/perfil={perfil}" , method = RequestMethod.GET)
    public ModelAndView findAllByPerfil(@PathVariable("perfil") String perfil) {
        ModelAndView mv = new ModelAndView("/Usuario/list");
        mv.addObject("usuarios", service.findByPerfil(perfil));
        mv.addObject("perfis", service.getPerfis());
        mv.addObject("cursos", service.getCursos());
        mv.addObject("turmas", service.getTurmas());
        mv.addObject("setores", service.getSetores());
        return mv;
    }

    @GetMapping("/Usuario/add")
    public ModelAndView add(Usuario usuario) {
        ModelAndView mv = new ModelAndView("/Usuario/edit");
        Perfil p = new Perfil();
        mv.addObject("usuario", usuario);
        mv.addObject("perfis", service.getPerfis());
        mv.addObject("cursos", service.getCursos());
        mv.addObject("turmas", service.getTurmas());
        mv.addObject("setores", service.getSetores());
        return mv;
    }

    @GetMapping("/Usuario/show/{id}")
    public ModelAndView show(@PathVariable("id") Long id) {
        Usuario usuario = service.show(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        ModelAndView mv = new ModelAndView("/Usuario/show");
        mv.addObject("usuario", usuario);
        return mv;

    }

    @GetMapping("/Usuario/edit/{id}")
    public ModelAndView update(@PathVariable("id") Long id) {
        Usuario usuario = service.findOne(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
            return add(usuario);
    }

    @GetMapping("/Usuario/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        service.delete(id);
        return findAll();
    }

    @PostMapping("/Usuario/save")
    public ModelAndView save(@RequestParam("perfis") String perfis, @Valid Usuario usuario, BindingResult result) {
        if(result.hasErrors()) {
            return add(usuario);
        }
        service.save(usuario);
        return findAll();
    }

    //LOGIN

    /*
    @RequestMapping(value = "/user/registration", method = RequestMethod.GET)
    public String showRegistrationForm(WebRequest request, Model model) {
        Usuario usuario = new Usuario();
        model.addAttribute("user", usuario);
        return "registration";
    }

    public ModelAndView registerUsuarioAccount(
            @ModelAttribute("user") @Valid Usuario accountDto,
            BindingResult result, WebRequest request, Errors errors) {
        System.out.println("Deu ruim");
        }

    }

     */
/*
    @Target({TYPE, FIELD, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = EmailValidator.class)
    @Documented
    public @interface ValidEmail {
        String message() default "Invalid email";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    public class EmailValidator
            implements ConstraintValidator<ValidEmail, String> {

        private Pattern pattern;
        private Matcher matcher;
        private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$";

        @Override
        public void initialize(ValidEmail constraintAnnotation) {
        }

        @Override
        public boolean isValid(String email, ConstraintValidatorContext context) {
            return (validateEmail(email));
        }

        private boolean validateEmail(String email) {
            pattern = Pattern.compile(EMAIL_PATTERN);
            matcher = pattern.matcher(email);
            return matcher.matches();
        }
    }

    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = PasswordMatchesValidator.class)
    @Documented
    public @interface PasswordMatches {
        String message() default "Passwords don't match";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    public class PasswordMatchesValidator
            implements ConstraintValidator<PasswordMatches, Object> {

        @Override
        public void initialize(PasswordMatches constraintAnnotation) {
        }

        @Override
        public boolean isValid(Object obj, ConstraintValidatorContext context) {
            Usuario user = (Usuario) obj;
            return user.getSenha().equals(user.getMatchingPassword());
        }

    }

    @RequestMapping(value = "/user/registration", method = RequestMethod.POST)
    public ModelAndView registerUsuarioAccount
            (@ModelAttribute("user") @Valid Usuario accountDto,
             BindingResult result, WebRequest request, Errors errors) {
        Usuario registered = new Usuario();
        if (!result.hasErrors()) {
            registered = createUsuarioAccount(accountDto, result);
        }
        if (registered == null) {
            result.rejectValue("email", "message.regError");
        }
        // rest of the implementation
    }

    private Usuario createUsuarioAccount(Usuario accountDto, BindingResult result) {
        Usuario registered = null;
        try {
            registered = service.registerNewUsuarioAccount(accountDto);
        } catch (EmailExistsException e) {
            return null;
        }
        return registered;
    }

 */
}