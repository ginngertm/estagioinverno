package uft.edu.br.estagio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import uft.edu.br.estagio.model.Estagio;
import uft.edu.br.estagio.service.EstagioService;

import javax.validation.Valid;


@Controller
public class EstagioController {

    @Autowired
    private EstagioService service;
    

/*
    @Autowired
    private TipoEstagioService tipoEstagioService;
    private Map<Long, TipoEstagio> tiposEstagio = new HashMap<>();

    @Autowired
    private CursoService cursoService;
    private Map<Long, Curso> cursos = new HashMap<>();
    
     */

    @GetMapping("/Estagio/list")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("/Estagio/list");
        mv.addObject("estagios", service.findAll());
        mv.addObject("alunos", service.getAlunos());
        mv.addObject("supervisores", service.getSupervisores());
      //  mv.addObject("cursos", service.getCursos());
        return mv;
    }

    /*
    @RequestMapping(path="/Estagio/findBy/perfil={perfil}" , method = RequestMethod.GET)
    public ModelAndView findAllByTipoEstagio(@PathVariable("perfil") String perfil) {
        ModelAndView mv = new ModelAndView("/Estagio/list");
        mv.addObject("estagios", service.findByTipoEstagio(perfil));
     //   mv.addObject("tiposEstagio", service.getTiposEstagio());
     //   mv.addObject("cursos", service.getCursos());
        return mv;
    }

     */

    @GetMapping("/Estagio/add")
    public ModelAndView add(Estagio estagio) {
        ModelAndView mv = new ModelAndView("/Estagio/edit");
        mv.addObject("estagio", estagio);
        mv.addObject("alunos", service.getAlunos());
        mv.addObject("supervisores", service.getSupervisores());
     //   mv.addObject("cursos", service.getCursos());
        return mv;
    }

    @GetMapping("/Estagio/show/{id}")
    public ModelAndView show(@PathVariable("id") Long id) {
        Estagio estagio = service.show(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid estagio Id:" + id));
        ModelAndView mv = new ModelAndView("/Estagio/show");
        mv.addObject("estagio", estagio);
        mv.addObject("alunos", service.getAlunos());
        mv.addObject("supervisores", service.getSupervisores());
        return mv;

    }

    @GetMapping("/Estagio/edit/{id}")
    public ModelAndView update(@PathVariable("id") Long id) {
        Estagio estagio = service.findOne(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid estagio Id:" + id));
            return add(estagio);
    }

    @GetMapping("/Estagio/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        service.delete(id);
        return findAll();
    }

    @PostMapping("/Estagio/save")
    public ModelAndView save( @RequestParam("alunos") String alunos, @RequestParam("supervisores") String supervisores, @Valid Estagio estagio, BindingResult result) {
        if(result.hasErrors()) {
            return add(estagio);
        }
        service.save(estagio);
        return findAll();
    }

}