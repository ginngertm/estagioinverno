package uft.edu.br.estagio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import uft.edu.br.estagio.model.Permissao;
import uft.edu.br.estagio.service.PermissaoService;

import javax.validation.Valid;


@Controller
public class PermissaoController {


    @Autowired
    private PermissaoService service;

    @GetMapping("/Permissao/list")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("/Permissao/list");
        mv.addObject("permissoes", service.findAll());
        mv.addObject("perfis", service.getPerfis());
        return mv;
    }

    @GetMapping("/Permissao/add")
    public ModelAndView add(Permissao permissao) {
        ModelAndView mv = new ModelAndView("/Permissao/edit");
        mv.addObject("permissao", permissao);
        mv.addObject("perfis", service.getPerfis());

        return mv;
    }

    @GetMapping("/Permissao/show/{id}")
    public ModelAndView show(@PathVariable("id") Long id) {
        Permissao permissao = service.show(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        ModelAndView mv = new ModelAndView("/Permissao/show");
        mv.addObject("permissao", permissao);
        mv.addObject("perfis", service.getPerfis());

        return mv;

    }

    @GetMapping("/Permissao/edit/{id}")
    public ModelAndView update(@PathVariable("id") Long id) {
        Permissao permissao = service.findOne(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
            return add(permissao);
    }

    @GetMapping("/Permissao/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        service.delete(id);
        return findAll();
    }

    @PostMapping("/Permissao/save")
    public ModelAndView save(@Valid Permissao permissao, BindingResult result) {
        if(result.hasErrors()) {
            return add(permissao);
        }
        service.save(permissao);
        return findAll();
    }

}