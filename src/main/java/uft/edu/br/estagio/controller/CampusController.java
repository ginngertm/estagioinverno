package uft.edu.br.estagio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import uft.edu.br.estagio.model.Campus;
import uft.edu.br.estagio.service.CampusService;

import javax.validation.Valid;


@Controller
public class CampusController {


    @Autowired
    private CampusService service;


    @GetMapping("/Campus/list")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("/Campus/list");
        mv.addObject("campi", service.findAll());
        mv.addObject("entidades", service.getEntidades());
        mv.addObject("cursos", service.getCursos());
        mv.addObject("setores", service.getSetores());

        return mv;
    }

    @GetMapping("/Campus/add")
    public ModelAndView add(Campus campus) {
        ModelAndView mv = new ModelAndView("/Campus/edit");
        mv.addObject("campus", campus);
        mv.addObject("entidades", service.getEntidades());
        mv.addObject("cursos", service.getCursos());
        mv.addObject("setores", service.getSetores());


        return mv;
    }

    @GetMapping("/Campus/show/{id}")
    public ModelAndView show(@PathVariable("id") Long id) {
        Campus campus = service.show(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        ModelAndView mv = new ModelAndView("/Campus/show");
        mv.addObject("campus", campus);
        return mv;

    }

    @GetMapping("/Campus/edit/{id}")
    public ModelAndView update(@PathVariable("id") Long id) {
        Campus campus = service.findOne(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
            return add(campus);
    }

    @GetMapping("/Campus/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        service.delete(id);
        return findAll();
    }

    @PostMapping("/Campus/save")
    public ModelAndView save(@RequestParam("entidades") String entidades, @Valid Campus campus, BindingResult result) {
        if(result.hasErrors()) {
            return add(campus);
        }
        service.save(campus);
        return findAll();
    }
}