package uft.edu.br.estagio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import uft.edu.br.estagio.model.Curso;
import uft.edu.br.estagio.service.CursoService;

import javax.validation.Valid;


@Controller
public class CursoController {

    @Autowired
    private CursoService service;
    

/*
    @Autowired
    private TipoCursoService tipoCursoService;
    private Map<Long, TipoCurso> tiposCurso = new HashMap<>();

    @Autowired
    private CursoService cursoService;
    private Map<Long, Curso> cursos = new HashMap<>();
    
     */

    @GetMapping("/Curso/list")
    public ModelAndView findAll() {
        ModelAndView mv = new ModelAndView("/Curso/list");
        mv.addObject("cursos", service.findAll());
        mv.addObject("campi", service.getCampi());
      //  mv.addObject("cursos", service.getCursos());
        return mv;
    }

    /*
    @RequestMapping(path="/Curso/findBy/perfil={perfil}" , method = RequestMethod.GET)
    public ModelAndView findAllByTipoCurso(@PathVariable("perfil") String perfil) {
        ModelAndView mv = new ModelAndView("/Curso/list");
        mv.addObject("cursos", service.findByTipoCurso(perfil));
     //   mv.addObject("tiposCurso", service.getTiposCurso());
     //   mv.addObject("cursos", service.getCursos());
        return mv;
    }

     */

    @GetMapping("/Curso/add")
    public ModelAndView add(Curso curso) {
        ModelAndView mv = new ModelAndView("/Curso/edit");
        mv.addObject("curso", curso);
        mv.addObject("campi", service.getCampi());
     //   mv.addObject("cursos", service.getCursos());
        return mv;
    }

    @GetMapping("/Curso/show/{id}")
    public ModelAndView show(@PathVariable("id") Long id) {
        Curso curso = service.show(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid curso Id:" + id));
        ModelAndView mv = new ModelAndView("/Curso/show");
        mv.addObject("curso", curso);
        return mv;
    }

    @GetMapping("/Curso/edit/{id}")
    public ModelAndView update(@PathVariable("id") Long id) {
        Curso curso = service.findOne(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid curso Id:" + id));
            return add(curso);
    }

    @GetMapping("/Curso/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        service.delete(id);
        return findAll();
    }

    @PostMapping("/Curso/save")
   // public ModelAndView save(@RequestParam("tiposCurso") String tiposCurso, @RequestParam("cursos") String cursos, @Valid Curso curso, BindingResult result) {
    public ModelAndView save(@RequestParam("campi") String campi, @Valid Curso curso, BindingResult result) {
        if(result.hasErrors()) {
            return add(curso);
        }
        service.save(curso);
        return findAll();
    }

}