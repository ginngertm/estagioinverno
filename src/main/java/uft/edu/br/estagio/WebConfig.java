package uft.edu.br.estagio;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(
                "/webjars/**",
                "/plugins/**",
                "/img/**",
                "/css/**",
                "/webfonts/**",
                "/js/**",
                "/**")
                .addResourceLocations(
                        "classpath:/META-INF/resources/webjars/",
                        "classpath:/static/plugins/",
                        "classpath:/static/img/",
                        "classpath:/static/css/",
                        "classpath:/static/webfonts",
                        "classpath:/static/js/",
                        "classpath:/static/js/",
                        "classpath:/**");


        WebMvcConfigurer.super.addResourceHandlers(registry);
    }


    @Override
    public void configureDefaultServletHandling(
            DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }


}